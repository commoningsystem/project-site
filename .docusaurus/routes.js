
import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';
export default [
{
  path: '/',
  component: ComponentCreator('/','deb'),
  exact: true,
},
{
  path: '/markdown-page',
  component: ComponentCreator('/markdown-page','be1'),
  exact: true,
},
{
  path: '/docs',
  component: ComponentCreator('/docs','f3b'),
  
  routes: [
{
  path: '/docs/anliegen',
  component: ComponentCreator('/docs/anliegen','3ea'),
  exact: true,
},
{
  path: '/docs/aufwand-und-einheitsbestimmung',
  component: ComponentCreator('/docs/aufwand-und-einheitsbestimmung','3e9'),
  exact: true,
},
{
  path: '/docs/ausdehnungsdrang',
  component: ComponentCreator('/docs/ausdehnungsdrang','a34'),
  exact: true,
},
{
  path: '/docs/beteiligungsmoeglichkeiten',
  component: ComponentCreator('/docs/beteiligungsmoeglichkeiten','7c3'),
  exact: true,
},
{
  path: '/docs/bibliothek',
  component: ComponentCreator('/docs/bibliothek','6b4'),
  exact: true,
},
{
  path: '/docs/codefrage',
  component: ComponentCreator('/docs/codefrage','c5e'),
  exact: true,
},
{
  path: '/docs/commoning-definition',
  component: ComponentCreator('/docs/commoning-definition','26d'),
  exact: true,
},
{
  path: '/docs/commons-und-sprache',
  component: ComponentCreator('/docs/commons-und-sprache','e29'),
  exact: true,
},
{
  path: '/docs/danksagungen',
  component: ComponentCreator('/docs/danksagungen','113'),
  exact: true,
},
{
  path: '/docs/datenstruktur',
  component: ComponentCreator('/docs/datenstruktur','af2'),
  exact: true,
},
{
  path: '/docs/designprinzipien',
  component: ComponentCreator('/docs/designprinzipien','249'),
  exact: true,
},
{
  path: '/docs/einfuehrung-timeless-way',
  component: ComponentCreator('/docs/einfuehrung-timeless-way','293'),
  exact: true,
},
{
  path: '/docs/einfuehrungen-beschreibung',
  component: ComponentCreator('/docs/einfuehrungen-beschreibung','8e6'),
  exact: true,
},
{
  path: '/docs/Einführungen/Auswahl/Website-Beschreibung',
  component: ComponentCreator('/docs/Einführungen/Auswahl/Website-Beschreibung','ec8'),
  exact: true,
},
{
  path: '/docs/Einführungen/Bestand/Barcamp Einführung',
  component: ComponentCreator('/docs/Einführungen/Bestand/Barcamp Einführung','030'),
  exact: true,
},
{
  path: '/docs/Einführungen/Bestand/Poträt Nachbarschaftswirtschaft',
  component: ComponentCreator('/docs/Einführungen/Bestand/Poträt Nachbarschaftswirtschaft','df9'),
  exact: true,
},
{
  path: '/docs/Einführungen/Videos/Issmit Kurzvortrag',
  component: ComponentCreator('/docs/Einführungen/Videos/Issmit Kurzvortrag','beb'),
  exact: true,
},
{
  path: '/docs/Einführungen/Videos/Vortrag zur Systematik',
  component: ComponentCreator('/docs/Einführungen/Videos/Vortrag zur Systematik','9d3'),
  exact: true,
},
{
  path: '/docs/einstiegsprozess',
  component: ComponentCreator('/docs/einstiegsprozess','5c5'),
  exact: true,
},
{
  path: '/docs/entfaltungsnetz',
  component: ComponentCreator('/docs/entfaltungsnetz','b90'),
  exact: true,
},
{
  path: '/docs/essays-uebersicht-und-anhang',
  component: ComponentCreator('/docs/essays-uebersicht-und-anhang','5d9'),
  exact: true,
},
{
  path: '/docs/faehigkeiten',
  component: ComponentCreator('/docs/faehigkeiten','e3d'),
  exact: true,
},
{
  path: '/docs/festsetzen-von-konfigurationen',
  component: ComponentCreator('/docs/festsetzen-von-konfigurationen','938'),
  exact: true,
},
{
  path: '/docs/gcs-drittes-vorwort',
  component: ComponentCreator('/docs/gcs-drittes-vorwort','494'),
  exact: true,
},
{
  path: '/docs/gcs-erstes-vorwort',
  component: ComponentCreator('/docs/gcs-erstes-vorwort','688'),
  exact: true,
},
{
  path: '/docs/gcs-konzept-uebersicht',
  component: ComponentCreator('/docs/gcs-konzept-uebersicht','d16'),
  exact: true,
},
{
  path: '/docs/gcs-zweites-vorwort',
  component: ComponentCreator('/docs/gcs-zweites-vorwort','fda'),
  exact: true,
},
{
  path: '/docs/glossar',
  component: ComponentCreator('/docs/glossar','a17'),
  exact: true,
},
{
  path: '/docs/grundlagen-softwarestruktur',
  component: ComponentCreator('/docs/grundlagen-softwarestruktur','31a'),
  exact: true,
},
{
  path: '/docs/individueller-musterspeicher',
  component: ComponentCreator('/docs/individueller-musterspeicher','2d6'),
  exact: true,
},
{
  path: '/docs/interaktion',
  component: ComponentCreator('/docs/interaktion','0de'),
  exact: true,
},
{
  path: '/docs/issmit-app',
  component: ComponentCreator('/docs/issmit-app','1dd'),
  exact: true,
},
{
  path: '/docs/issmit-crowdfunding',
  component: ComponentCreator('/docs/issmit-crowdfunding','3f1'),
  exact: true,
},
{
  path: '/docs/keimformtheorie',
  component: ComponentCreator('/docs/keimformtheorie','2e0'),
  exact: true,
},
{
  path: '/docs/konfigurationen',
  component: ComponentCreator('/docs/konfigurationen','001'),
  exact: true,
},
{
  path: '/docs/konfigurationsprozess',
  component: ComponentCreator('/docs/konfigurationsprozess','840'),
  exact: true,
},
{
  path: '/docs/kontakt',
  component: ComponentCreator('/docs/kontakt','c76'),
  exact: true,
},
{
  path: '/docs/kontinuitaet',
  component: ComponentCreator('/docs/kontinuitaet','32d'),
  exact: true,
},
{
  path: '/docs/kurzbeschreibung',
  component: ComponentCreator('/docs/kurzbeschreibung','bcd'),
  exact: true,
},
{
  path: '/docs/Lizenzen',
  component: ComponentCreator('/docs/Lizenzen','66d'),
  exact: true,
},
{
  path: '/docs/manifest-gegen-die-arbeit',
  component: ComponentCreator('/docs/manifest-gegen-die-arbeit','c6f'),
  exact: true,
},
{
  path: '/docs/momente-des-gesamtaufwandes',
  component: ComponentCreator('/docs/momente-des-gesamtaufwandes','71a'),
  exact: true,
},
{
  path: '/docs/plankonfigurationen',
  component: ComponentCreator('/docs/plankonfigurationen','e22'),
  exact: true,
},
{
  path: '/docs/podcast',
  component: ComponentCreator('/docs/podcast','315'),
  exact: true,
},
{
  path: '/docs/projekt-entstehung',
  component: ComponentCreator('/docs/projekt-entstehung','ddd'),
  exact: true,
},
{
  path: '/docs/qualifikationen',
  component: ComponentCreator('/docs/qualifikationen','925'),
  exact: true,
},
{
  path: '/docs/reparaturprozess',
  component: ComponentCreator('/docs/reparaturprozess','743'),
  exact: true,
},
{
  path: '/docs/strukturformel',
  component: ComponentCreator('/docs/strukturformel','66d'),
  exact: true,
},
{
  path: '/docs/taetigkeitsmuster',
  component: ComponentCreator('/docs/taetigkeitsmuster','572'),
  exact: true,
},
{
  path: '/docs/team-kommunikation',
  component: ComponentCreator('/docs/team-kommunikation','096'),
  exact: true,
},
{
  path: '/docs/theorie-distro',
  component: ComponentCreator('/docs/theorie-distro','2fe'),
  exact: true,
},
{
  path: '/docs/theorie-vorwissen-1',
  component: ComponentCreator('/docs/theorie-vorwissen-1','b69'),
  exact: true,
},
{
  path: '/docs/timeless-way-of-re-production',
  component: ComponentCreator('/docs/timeless-way-of-re-production','215'),
  exact: true,
},
{
  path: '/docs/uebersicht',
  component: ComponentCreator('/docs/uebersicht','851'),
  exact: true,
},
{
  path: '/docs/umfeldanalyse',
  component: ComponentCreator('/docs/umfeldanalyse','fae'),
  exact: true,
},
{
  path: '/docs/verhaltenskodex',
  component: ComponentCreator('/docs/verhaltenskodex','8c4'),
  exact: true,
},
]
},
{
  path: '*',
  component: ComponentCreator('*')
}
];
