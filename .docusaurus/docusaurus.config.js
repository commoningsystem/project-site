export default {
  "title": "Commoning System",
  "tagline": "komplexe Kooperation auf Augenhöhe",
  "url": "https://gitlab.com/commoningsystem/",
  "baseUrl": "/",
  "onBrokenLinks": "throw",
  "onBrokenMarkdownLinks": "warn",
  "favicon": "img/Mustericons/mustericon-alpha.ico",
  "organizationName": "commoningsystem",
  "projectName": "gcs-project",
  "themeConfig": {
    "navbar": {
      "title": "GCS-Project",
      "logo": {
        "alt": "GCS-Logo",
        "src": "img/Mustericons/Infrastruktur-ink-gelb.svg"
      },
      "items": [
        {
          "to": "/docs/uebersicht",
          "label": "Übersicht",
          "position": "left"
        },
        {
          "to": "/docs/anliegen",
          "label": "Anliegen",
          "position": "left"
        },
        {
          "to": "/docs/kurzbeschreibung",
          "label": "Einführung",
          "position": "left"
        },
        {
          "to": "/docs/datenstruktur",
          "label": "Datenstruktur",
          "position": "left"
        },
        {
          "to": "/docs/gcs-konzept-uebersicht",
          "label": "Konzept",
          "position": "left"
        },
        {
          "to": "/docs/glossar",
          "label": "Glossar",
          "position": "left"
        },
        {
          "href": "https://commoningsystem.org",
          "label": "Shell",
          "position": "right"
        },
        {
          "href": "https://community.commoningsystem.org",
          "label": "Forum",
          "position": "right"
        },
        {
          "href": "/docs/codefrage",
          "label": "Gitlab",
          "position": "right"
        }
      ],
      "hideOnScroll": false
    },
    "footer": {
      "style": "dark",
      "links": [
        {
          "title": "Team",
          "items": [
            {
              "label": "Konferenzraum",
              "to": "https://meet.ffmuc.net/globalcommoningsystem"
            },
            {
              "label": "Traktanden",
              "to": "https://pad.riseup.net/p/Transcomm-keep"
            },
            {
              "label": "Forum",
              "to": "https://community.commoningsystem.org"
            },
            {
              "label": "Nextcloud",
              "href": "https://sks.swisscloudhosting.ch/apps/files/?dir=/Global%20Commoning%20System&fileid=1452"
            }
          ]
        },
        {
          "title": "Fediverse",
          "items": [
            {
              "label": "[matrix]",
              "href": "https://matrix.to/#/#gcs-welcome:systemausfall.org?via=systemausfall.org&via=matrix.org&via=tchncs.de"
            },
            {
              "label": "Mastodon",
              "href": "https://chaos.social/@commoningsystem"
            },
            {
              "label": "Peer-Tube",
              "to": "https://tube.tchncs.de/accounts/commoning_system/video-channels"
            },
            {
              "label": "Funkwhale",
              "to": "https://open.audio/channels/gcs_werkstattbericht/"
            }
          ]
        },
        {
          "title": "Umfeld",
          "items": [
            {
              "label": "Commons Institut",
              "href": "https://commons-institut.org/"
            },
            {
              "label": "Keimform-Blog",
              "to": "https://www.keimform.de"
            },
            {
              "label": "P2P-Foundation",
              "to": "https://p2pfoundation.net/"
            }
          ]
        }
      ],
      "copyright": "Website steht unter der CC BY-SA 4.0+ Lizenz."
    },
    "colorMode": {
      "defaultMode": "light",
      "disableSwitch": false,
      "respectPrefersColorScheme": false,
      "switchConfig": {
        "darkIcon": "🌜",
        "darkIconStyle": {},
        "lightIcon": "🌞",
        "lightIconStyle": {}
      }
    },
    "docs": {
      "versionPersistence": "localStorage"
    },
    "metadatas": [],
    "prism": {
      "additionalLanguages": []
    },
    "hideableSidebar": false
  },
  "presets": [
    [
      "@docusaurus/preset-classic",
      {
        "docs": {
          "sidebarPath": "/home/thyseus/dev/project-site/sidebars.js",
          "editUrl": "https://gitlab.com/commoningsystem/docusaurus/edit/master/website/"
        },
        "blog": {
          "showReadingTime": true,
          "editUrl": "https://gitlab.com/commoningsystem/docusaurus/edit/master/website/blog/"
        },
        "theme": {
          "customCss": "/home/thyseus/dev/project-site/src/css/custom.css"
        }
      }
    ]
  ],
  "baseUrlIssueBanner": true,
  "i18n": {
    "defaultLocale": "en",
    "locales": [
      "en"
    ],
    "localeConfigs": {}
  },
  "onDuplicateRoutes": "warn",
  "customFields": {},
  "plugins": [],
  "themes": [],
  "titleDelimiter": "|",
  "noIndex": false
};
