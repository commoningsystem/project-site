import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

import useThemeContext from '@theme/hooks/useThemeContext';



const FeatureList = [
  {
    title: 'Das Gemeinsame organisieren',
    Svg: require('../../static/img/Mustericons/gemeinsam_organisieren-text_light.svg').default,
    description: (
      <>
        Was von anderen mitverwendet werden darf, in komplexe Prozesse
        zur höchstmöglichen Bedürfnisbefriedigung integrieren
      </>
    ),
  },
  {
    title: 'Gemeinsames schaffen',
    Svg: require('../../static/img/Mustericons/gemeinsam_erzeugen-text_light.svg').default,
    description: (
      <>
      Zur Erweiterung des Gemeinsamen beitragen und von anderen dabei unterstützt werden.
      </>
    ),
  },
  {
    title: 'Im Gemeinsamen leben',
    Svg: require('../../static/img/Mustericons/gemeinsames_leben-text_light.svg').default,
    description: (
      <>
        Die kleinen Probleme des Lebens und die großen Probleme der Menschheit gemeinsam angehen können. 
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
    <div className="text--center">
       <Svg className={styles.featureSvg} alt={title} />
     </div>
        <div className="text--center padding-horiz--md">

        <p>{description}</p>
      </div>



    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
