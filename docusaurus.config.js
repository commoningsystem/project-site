/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Commoning System',
  tagline: 'Komplexe Kooperation auf Augenhöhe',
  url: 'https://gitlab.com/commoningsystem/',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/Mustericons/mustericon-alpha.ico',
  organizationName: 'commoningsystem', // Usually your GitHub org/user name.
  projectName: 'gcs-project', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'GCS-Project',
      logo: {
        alt: 'GCS-Logo',
        src: 'img/Mustericons/Infrastruktur-ink-gelb.svg',
      },
      items: [
        {to: '/docs/uebersicht', label: 'Inhalt', position: 'left'},
        {
         href: 'https://commoningsystem.org',
         label: 'Shell',
         position: 'right',
        },
        {
          href: 'https://community.commoningsystem.org',
          label: 'Forum',
          position: 'right',
        },
        {
          href: 'https://gitlab.com/commoningsystem',
          label: 'Gitlab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Team',
          items: [
                        {
              label: 'Konferenzraum',
              to: 'https://meet.ffmuc.net/globalcommoningsystem',
            },
            {
              label: 'Traktanden',
              to: 'https://pad.riseup.net/p/Transcomm-keep',
            },
            {
              label: 'Forum',
              to: 'https://community.commoningsystem.org',
            },
              {
              label: 'Nextcloud',
              href: 'https://sks.swisscloudhosting.ch/apps/files/?dir=/Global%20Commoning%20System&fileid=1452',
            },

          ],
        },
        {
          title: 'Fediverse',
          items: [
            {
              label: '[matrix]',
              href: 'https://matrix.to/#/#gcs-welcome:systemausfall.org?via=systemausfall.org&via=matrix.org&via=tchncs.de',
            },


            {
              label: 'Mastodon',
              href: 'https://chaos.social/@commoningsystem',
            },
            {
              label: 'Peer-Tube',
              to: 'https://tube.tchncs.de/accounts/commoning_system/video-channels',
            },
            {
              label: 'Funkwhale',
              to: 'https://open.audio/channels/gcs_werkstattbericht/',
            },

          ],
        },
        {
          title: 'Umfeld',
          items: [
                        {
              label: 'Commons Institut',
              href: 'https://commons-institut.org/',
            },
            {
              label: 'Keimform-Blog',
              to: 'https://www.keimform.de',
            },
            {
              label: 'P2P-Foundation',
              to: 'https://p2pfoundation.net/',
            },

          ],
        },
      ],
      copyright: `Website steht unter der CC BY-SA 4.0+ Lizenz.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/commoningsystem/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/commoningsystem/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
