---
sidebar_position: 6
---

Global Commoning System - Eine richtungsweisende Freie Software entsteht
Aktivist Robert und Marcus vom Global Commoning System-Team erzählen übers Projekt und seine Herausforderungen und freuen sich über aktive Diskussionspartner*innen. mail@commoningsystem.org 

Die Covid- & Klima-Krise zeigt: Die Ökonomie muss wieder lokaler werden und der Suffizienzgedanke muss im Vordergrund stehen. Commons-Aktivist*innen haben sich deshalb zusammen getan und wollen durch eine Freie Software (Open-Source) die Art und Weise, wie gewirtschaftet wird, verändern. Das Ziel: Einen grundlegendes Softwareprotokoll erschaffen, das es Leuten, die sich bislang nicht kennen, ermöglicht, ihre jeweils eigenen Bedürfnisse zu vermitteln und diese Bedürfnisse schließlich gemeinsam zu befriedigen. 

In der fertig ausgebauten Version können hierbei komplexe arbeitsteilige Produktionsschritte entstehen, zu Beginn wird sich mit einer ersten App namens Issmit.app darauf konzentriert, eine Community in Pilotregionen aufzubauen, die freiwillig miteinander und füreinander kocht. Wer was wann macht, wird dabei nicht ausdiskutiert, sondern ergibt sich aus einer Selbstzuordnung zu von der Software vorgeschlagenen Tätigkeiten, die anhand der lokalen Verfügbarkeit von Lebensmitteln, Transportmöglichkeiten und Verarbeitungsgeräten ausgewählt werden. Zugrunde liegen die Wünsche der Beteiligten nach beispielsweise einem warmen Essen über Mittag. 

Das im Hintergrund arbeitende Softwareprotokoll ist hochkomplex, es muss verschiedene Produktions- & Transportketten inklusive Beschreibungen abbilden können. Die Anwendungssoftware hingegen wird so simpel zu gebrauchen sein wie eine Dating-App. Das Global Commoning System hat das Potenzial, das bisherige Wirtschaften wieder auf die Füße zu stellen: Zuerst kommt das Bedürfnis, dann die Produktion - nicht umgekehrt.

*Raffael Wüthrich, 2021*