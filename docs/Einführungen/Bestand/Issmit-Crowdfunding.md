---
sidebar_position: 4
slug: /issmit-crowdfunding
---

Die issmit.app: Deine Nachbarschaft kocht neuerdings füreinander!
Stell dir vor: Du kannst in einer neuen kostenlosen App eintragen, dass du an deinen Bürotagen jeweils ein gutes Essen haben möchtest, gekocht von Freiwilligen aus der Nachbarschaft. Im Hintergrund zerlegt die App deinen Wunsch in verschiedene Tätigkeitsschritte und zeigt die nötigen Arbeiten, Transporte, Zutaten, etc. wiederum in der App an. Leute, die die App ebenfalls benutzen, können sich nun für diese und andere Tätigkeiten einschreiben und gemeinsam dazu beitragen, dass die Nachbarschaft füreinander kocht. Je mehr Leute mitmachen, desto schneller wird dein Wunsch nach einem nachbarschaftlichen Essen an deinen Bürotagen erfüllt…

Weshalb dies Menschen tun sollten? Weils erstens Spass macht, anderen eine Freude zu bereiten. Zweitens, weil so neue Bekanntschaften im Quartier entstehen. Und weil drittens Mitmachende in der App Anerkennung für ihre Arbeit erhalten, was wiederum die Wahrscheinlichkeit erhöhen kann, dass ihre Wünsche ebenfalls erhört werden.

Die issmit.app ist eine völlig neue Art, deine Nachbarschaft kennenzulernen, tätig zu sein und zu liebevoll zubereitetem Essen zu kommen.

Damit wir mit der Programmierung der issmit.app starten können, brauchen wir deine Beteiligung. Erreichen wir das Ziel von CHF 7’700.- können unsere Software-Profis sofort mit den Programmierarbeiten loslegen. Noch dieses Jahr starten dann die Pilotprojekte, vielleicht auch in deiner Nachbarschaft?!

Je mehr Menschen helfen, dieses innovative Projekt voranzubringen, desto grösser wird die Chance, dass auch konventionelle Förder-Organisationen die issmit.app finanziell unterstützen. Zögere also nicht zu lange, trage zur Finanzierung bei und lerne deine Nachbarschaft beim Kochen und Essen bald ganz neu kennen… ;-)


### **Das Global Commoning System & die issmit.app**

2015 hat die Stiftung für Konsumentenschutz das Repair Café Bern gegründet. Mittlerweile gibt es dank der Förderung des Konsumentenschutzes über 150 solcher freiwilliger Reparatur-Organisationen in der Schweiz. Im 2019 eröffnete der Konsumentenschutz die erfolgreiche LeihBar Bern. Nun hat sich der Konsumentenschutz mit progressiven Entwicklern und Entwicklerinnen aus der Gemeingüter-Bewegung zusammengetan. Nach dem gemeinsamen Reparieren und dem gemeinsamen Leihen steht nun das gemeinsame Erschaffen im Vordergrund.

Das Ziel: Ein grundlegendes Softwareprotokoll namens «Global Commoning System» erschaffen, das es Leuten, die sich bislang nicht kennen, ermöglicht, eingebrachte Wünsche der Community-Mitglieder gemeinsam zu erfüllen.

In der fertig ausgebauten Version der Software können die Wünsche umfangreich und dahinter komplexe arbeitsteilige Produktionsschritte nötig sein.

Das «Global Commoning System» hat das Potenzial, das bisherige Wirtschaften wieder auf die Füsse zu stellen: Zuerst kommt das Bedürfnis, dann die Produktion – nicht mehr umgekehrt.

Die issmit.app ist der erste Schritt. Mit Hilfe der App will der Konsumentenschutz Communities in Pilotregionen aufbauen, die freiwillig miteinander und füreinander kochen.

Stösst die issmit.app auf Zustimmung, werden weitere Anwendungen folgen, um immer komplexere Bedürfnisse abdecken zu können.

Die Software ist opensource, das heisst, dass der Quellcode offen ist und die issmit.app und das «Global Commoning System» von Programmiererinnen auf der ganzen Welt weiterentwickelt werden können. Die Software ist zudem so lizenziert, dass sie niemals kostenpflichtig werden kann und immer frei bleiben muss. Die issmit.app gehört dir und allen anderen, die sie nutzen, und keinem Konzern, der es auf deine Daten abgesehen hat.

### **App-Entwicklung ist teuer!**

Leider haben wir im Team nicht genügend Software-Entwickler und -Entwicklerinnen, um die issmit.app und die grundlegenden Funktionalitäten selbst zu programmieren. Uns fehlt zum Teil auch das spezifische Know-how. Wir arbeiten deshalb mit drei jeweils spezialisierten Softwarefirmen zusammen. Das wiederum ist aber teuer. Wir rechnen mit offerierten CHF 7000.- pro sogenanntem Software-Sprint. Ein solcher Sprint umfasst zwei Wochen, in denen genau definierte Software-Anforderungen programmiert werden.

Damit werden wir die grundlegendsten Funktionalitäten der issmit.app programmiert haben. Dies ist ein wichtiger erster Schritt, weil wir damit dir und der ganzen Welt zeigen können, welche unglaubliche Power im Konzept steckt. Dies wird dann hoffentlich einerseits mehr finanzielle Mittel und andererseits ehrenamtliche Software-Entwickler und -Programmiererinnen anlocken, damit die App immer besser wird…

Damit es gesagt ist: Das Budget für die Programmierung und Bekanntmachung der issmit.app beträgt über CHF 50’000.-. Diesen Betrag kann der Konsumentenschutz alleine nicht aufbringen. Das Crowdfunding ist deshalb die Startrakete auf dem Weg zur grösseren Finanzierung durch weitere Organisationen, entsprechende Anträge laufen. Dein Beitrag ist also enorm wichtig, um zu zeigen, dass die issmit.app auf Interesse stösst!

Wir hoffen auf deine Beteiligung bei der Finanzierung. Im Gegenzug stellen wir eine Software zur freien Verfügung, die zu einer besseren Welt beitragen kann.

Wenn du im Projekt mithelfen willst, melde dich! Wir freuen uns über deine Verstärkung.

*Raffael Wüthrich, 2021*
