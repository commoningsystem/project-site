---
sidebar_position: 1
slug: /projekt-entstehung
---

Eine einzige Frage hat zu diesem Softwareprojekt geführt: „Wenn sich sämtliche wesentlichen Momente der kapitalistischen Produktionsweise durch eine einzige Formel darstellen lassen (die **Zirkulationsformel** aus dem zweiten Band des marxschen Kapitals), ist es nicht auch möglich - wenn nicht sogar notwendig - den Prozess des Commonings durch eine allgemeingültige Formel zu beschreiben und dadurch Rückschlüsse auf seine Struktur auf gesamtgesellschaftlicher Ebene zu ziehen?“

Vielleicht nur ganz kurz und ohne näher darauf einzugehen, da die Zirkulationsformel im Internet wirklich schwierig zu finden ist:

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';


<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Formeln/kapitalistischer_zweifach.svg'),
    dark: useBaseUrl('/img/Formeln/kapitalistischer_zweifach_dark.svg'),
  }}
/>

Was dieser kapitalistische Produktionsprozess ist, ist entweder bekannt oder kann zum Beispiel in ["das Kapital und die Commons"](https://archive.org/details/daskapitalunddiecommons) nachgelesen werden. Wichtig allerdings ist die Frage: Was ist das dem **„…P…“** entsprechende **„…c…“**, wie kann der Prozess des Commonings auf ähnliche Weise gefasst werden? Und Commoning, an der Stelle angemerkt, ist ein sperriger Begriff für etwas, das selbstverständlich ist. Es bezeichnet den Prozess von gleichgestellten Menschen, die sich freiwillig selbst organisieren, um mit den ihnen zur Verfügung stehenden Mitteln ihre Bedürfnisse durch kooperative Tätigkeit zu befriedigen und dabei entstehende Konflikte auf Augenhöhe beziehungswahrend bearbeiten. Sprich: Weil wir als Menschen alle gleichermaßen und immer wieder Bedürfnisse haben, gehen wir nicht in Konkurrenz zueinander, sondern regeln untereinander, was es zu tun gibt, machen das und wenn es dabei zu Konflikten kommt, dann versuchen wir eine Lösung zu finden, mit der wir alle leben können. Mehr ist es nicht, aber auf  gesamtgesellschaftlicher Ebene doch schwierig umzusetzen.

Mit dem Softwareprojekt wird genau das versucht.

Die Grundlagen dafür wurden im [**Ausdehnungsdrang moderner Commons**](/docs/ausdehnungsdrang) gelegt - eine Bearbeitung des damals gerade erst erschienen Buches [**Kapitalismus aufheben**](https://commonism.us) von Simon Sutterlütti und Stefan Meretz. Von der **Kritischen Psychologie**ausgehend, haben Sutterlütti und Meretz sich nicht mit bloßen Revolutionsfloskeln abgefunden, sondern die Möglichkeit einer Transformation des Kapitalimus zur ersehnten Gesellschaft beschrieben, so weit es ihnen nur möglich war. Die beiden Autoren sind dabei zwar auf für sie nicht lösbare Probleme gestoßen, allerdings haben sie darin begriffliche Grundlagen geschaffen, um Transformation überhaupt sinnvoll denken zu können. So wäre auch das Softwareprojekt selbst nicht denkbar gewesen, wenn das Buch nicht damals erschienen wäre.

Was passiert im *Ausdehnungsdrang* Erst werden Probleme in *Kapitalismus aufheben* aufgezeigt, welche zu bestimmten Trugschlüssen führen, schließlich die kapitalistische Produktionsweise wiederholt - insoweit sie für die Transformation von Bedeutung ist - und letztendlich wird eine erste **Formel des Commonings** erarbeitet; keine allgemeine, sondern eine auf die Bedürfnisdimensionen zugeschnittene. Es wird weiter die These aufgestellt, dass Commoning, wenn es zwischen konkreten Personen betrieben wird, die in direkter Beziehung zueinander stehen, andersartig funktioniert muss, als wenn es von konkreten Personen betrieben wird die *nicht* in direkter Beziehung zueinander stehen. Wenn Personen *nicht* in direkter Beziehung zueinander stehen, dann wird, um Commoning effizient betreiben zu können,- so die These weiter - eine **gemeinsame Instanz zur Selbstorganisation und Zwecksetzung von Mitteln** benötigt, über welche diese Personen sich vermitteln. Im letzten Drittel des Essays schließlich, wird so eine *Instanz* als gegeben angenommen und es werden vier von Sutterlütti und Meretz angerissene Szenarien damit neu durchgegangen und gezeigt, welche Potentiale diese Instanz für eine Transformation in sich trägt.

Robert, ein Entwickler Freier Software aus dem Projekt Grouprise, hat schließlich eine nähere Diskussion zu dieser beschriebenen Instanz angestoßen, welche von hier ab schlicht als Software bzw. *die Software* bezeichnet wird. Aus einer gemeinsamen Diskussion heraus wurde bald offensichtlich, dass wir es mit der Ordnung von sich-wiederholenden Prozessen zu tun haben und daher mit *Mustern* arbeiten müssen - ein Thema das bereits Silke Helfrich und David Bollier in [**Die Welt der Commons - Muster des gemeinsamen Handelns**](https://library.oapen.org/handle/20.500.12657/31839) - wenn auch auf andere Weise - für das Commoning erschlossen haben und in [**Fair, Frei, Lebendig**](https://library.oapen.org/handle/20.500.12657/25047) weiterführten.

Der Essay [**The Timeless Way of Re-Production**](/docs/timeless-way-of-re-production) ist schließlich eine Interpretation von *The Timeless Way of Building* des Architekten und Mustertheoretikers Christopher Alexander. Alexander beschreibt darin, wie ein bestimmter Gegenstand (bei ihm die Architektur, bei uns der gesellschaftliche Re-Produktionsprozess) in Einzelteile zerlegt werden kann und wie es möglich ist, diesen Einzelteilen eine *allgemeingültige Qualität* zuzuschreiben, durch welche sich schließlich eine sinnvolle Ordnung dieser Einzelteile herstellen lässt - bei Alexander eine Ordnung Richtung Lebendigkeit, bei uns eine Ordnung in Richtung allgemeiner Bedürfnisbefriedigung. Diese einzelnen Teile - diese *Muster* - sind dabei sowohl Wissensspeicher als auch ein Medium, über welche eine gemeinsame Kommunikation und Planung auf Augenhöhe ermöglicht wird. Im Essay wird dabei dem Aufbau von *The Timeless Way of Building* gefolgt, direkt zitierte Abschnitte auf die Softwarekonzeption interpretiert und somit die grundlegende Struktur und notwendigen Funktionen der Software erschlossen.

Die Textreihe [**Ein Softwarekonzept für ununterbrochenes Commoning **](/docs/gcs-erstes-vorwort), welche durch den Essay unterbrochen wurde und bisher nicht abgeschlossen ist, konkretisiert einerseits die Erkenntnisse aus dem *Timeless Way*, vervollständigt dabei allerdings auch die Softwarestruktur. Die Textreihe soll zwar allgemein verständlich sein, richtet sich dabei aber in erster Linie an Entwickler und Entwicklerinnen, um dabei ein Leitfaden zur wirklichen Umsetzung des Softwareprojektes zu sein.

*Marcus Meindel, 2019 - Erschienen zur Beschreibung des Theorie-Bereiches im ehemaligen allmende.io-Forum*
