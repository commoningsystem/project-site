---
title: Über
sidebar_position: 0
slug: /einfuehrungen-beschreibung
---

Zu verschiedenen Zeiten, zu verschiedenen Anliegen wurden Einführungen zu unserem Projekt geschrieben. Unter **Auswahl** sind Einführungen gesammelt, die Interessierten den vielleicht leichtesten Einstieg geben. Unter **Bestand** sind Einführungen zu tendenziell spezifischeren Anlässen gesammelt. Unter **Videos** sind bisher erschienene Vorträge und der Animationsfilm.

Da die Einführungen zu verschiedenen Projektständen und verschiedenen Autoren geschrieben wurden, kann es zu Unterschieden zwischen ihnen bzw. zu Unterschieden zwischen den Einführungen und dem gegenwärtigen Stand des Konzeptes kommen. Leider ist das gerade nur schwer vermeidbar. 
