---
sidebar_position: 2
---

Das *Softwarekonzept für ununterbrochenes Commoning* entsteht im Rahmen der Arbeiten des Commons-Instituts und geht der Vision nach, das bisher
größte vom Menschen geschaffene Netzwerk der Welt abzulösen: den Markt.
Dabei setzt es auf das bisher zweitgrößte, inzwischen ebenfalls
weltumspannende Netzwerk: das Internet.

Alle Dinge, die wir uns für Geld aneignen können, sind Teil des Marktes.
Aber was ist mit den Dingen, die es nicht sind? Wenn etwas nicht
käuflich ist, aber so verwendet werden darf, dass es zum größten Vorteil
aller ist? Das ist ein *Commons*. Doch bei der Verwendung eines solchen
Commons zeigen sich schnell Grenzen. Es muss darüber diskutiert werden,
was die an der Verwendung Beteiligten gerade brauchen, wie es daher zum
größten Vorteil aller verwendet werden kann und schließlich müssen
Absprachen zur Verwendung davon getroffen werden. Und diese Absprachen
müssen allgemein einsichtig sein - es reicht nicht, wenn diese nur
einigen Personen bekannt ist oder Informationen darüber schwierig zu
finden sind. Und was, wenn es mehr solcher Dinge gibt, die zum größten
Vorteil aller verwendet werden können? Wenn es nicht nur der ein oder
andere Acker ist, sondern auch in Haushalten verteilte Gebrauchsgüter
mit einschließt oder größere Maschinen und vielleicht ganze Fabriken?
Dann kann über vereinzelte Absprachen und besonders auch arbeitsteilige
Prozesse, wie sie heute sehr oft notwendig sind, kaum Übersicht gehalten
werden. Über die *Software für ununterbrochenes Commoning* soll dieses
Problem gelöst werden.

Durch die Software wird ersichtlich, was „zum größten Vorteil aller“
bedeutet. Welche Dinge dafür zur Verfügung stehen und welche Absprachen
zu deren Verwendung bereits getroffen wurden. Es zeigt sich, welche
Strukturen sich bereits gebildet haben, in denen Menschen sich
anstehenden Bedürfnissen mit ihren eigenen Fähigkeiten und Interessen
annehmen und es soll möglich werden, dass diese Strukturen von allen
daran beteiligten Personen mitbestimmt werden können. In Echtzeit werden
darin sämtliche Informationen geteilt, die einander unbekannte Personen
und Gruppen benötigen, um gemeinsam zu arbeiten und sich gegenseitig das
bereitzustellen, was für die eigenen Tätigkeiten und für das eigene
Leben benötigt wird. Eine zentrale Instanz, welche mit Stift, Papier und
„weiser Voraussicht“ von oben herab plant, wird dabei überflüssig. Was
wir suchen und als Projektteam konstruieren, ist eine Form der
Selbstorganisation auf Augenhöhe, die unbegrenzt wachsen kann.

Selbstbestimmung wird heute durch den Markt stark eingeschränkt. Oft
arbeiten wir nur, um unsere laufenden Kosten zu zahlen und die Arbeit
wird bis zur Rente nicht weniger. Das liegt nicht daran, dass Arbeit zum
Leben dazu gehört. Das mag so sein, aber mit all den technischen
Errungenschaften, die wir alleine in den letzten hundert Jahren hatten,
sollte die Arbeit wenigstens deutlich weniger geworden sein. Die Arbeit
ist allerdings nicht weniger geworden und sie kann es auch nicht werden,
solange wir uns in Strukturen von Konkurrenz, Wettbewerb, Profit und
Wachstumszwang bewegen. Weil es nicht nur uns erschöpft, sondern auch
das Naturvermögen und damit unsere Lebensgrundlage notwendigerweise
zerstört, wollen wir dabei nicht länger mitmachen. Jenseits von Markt
und Staat gibt es eine Freiheit, die andere bei ihrer Ausübung nicht
einschränkt und in der wir zeitgemäß und mit der Natur im Einklang leben
können. Wir entwickeln diese Software, weil wir diese Freiheit suchen
und wir überzeugt davon sind, dass die Software ein Werkzeug ist, mit
der sie erreicht werden kann.

Mit der *Software für ununterbrochenes Commoning* soll daher eine
grundlegend nachhaltige Veränderung ermöglicht werden, die heute noch
jenseits des Machbaren erscheint.

*Christian Schorsch und Marcus Meindel, 2020*