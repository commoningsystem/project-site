---
sidebar_position: 1
slug: /kurzbeschreibung
---

Die Software ist eine neuartige Möglichkeit, sämtliche Tätigkeiten, welche zur Befriedigung menschlicher Bedürfnisse notwendig sind, auf Augenhöhe zu vermitteln und somit sowohl für andere direkt da zu sein, als auch sich in komplexe Produktionsprozesse einzubringen. Sobald Bedürfnisse vermittelt wurden, wird automatisch das lokale Umfeld nach verfügbaren Mitteln (Werkzeugen, Maschinen, Arbeitsmaterialien, Räumlichkeiten, etc.) analysiert und Benutzern mögliche Tätigkeiten angeboten, denen diese sich den eigenen Fähigkeiten und Interessen nach zuordnen können. Diese Tätigkeiten selbst sind Erfahrungen anderer Beteiligter, die einmal vor demselben Problem standen. Die Software kann herausstellen, welche Tätigkeiten im jeweiligen Umfeld am effizientesten dazu beitragen, möglichst viele unterschiedliche Bedürfnisse mit möglichst geringem Aufwand zu befriedigen und diese Informationen den Benutzern zur Verfügung stellen.

*Raffael Wüthrich, 2020*
