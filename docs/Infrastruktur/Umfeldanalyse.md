---
sidebar_position: 3
slug: /umfeldanalyse
---

Die Umfeldanalyse ist zur Zeit (30.6.21) eine laufende Aufgabe im Projekt. Sobald wir zu einem vorläufigen Abschluss gekommen sind, werden wir die Informationen an dieser Stelle teilen.

Falls du Vorschläge hast, nehm gerne mit uns [Kontakt](/docs/kontakt) auf.
