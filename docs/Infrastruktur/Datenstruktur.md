---
sidebar_position: 2
slug: /datenstruktur
---
import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';


Die folgende Datenstruktur zeigt unseren Projektstand Juni'21, sollte aber nicht als abgeschlossen betrachtet werden. Trotzdem gibt sie die für uns wichtigsten Punkte wider.

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Datenstruktur/ENGL_2021-datenmodell-light.svg'),
    dark: useBaseUrl('/img/Datenstruktur/ENGL_2021-datenmodell-dark.svg'),
  }}
/>

## Datenbanken

*(databases)*

Bisher unterscheiden wir zwischen vier verschiedenen Arten verteilter Datenbanken:

1. **Mittel-Datenbanken** (means databases): Hierin sind *Mittel* (Rasenmäher, Töpfe, Mehl, Öl, etc.) und verschiedene Formen von Meta-Daten (Menge, Zustand, Standort, etc.) festgehalten.
2. **Tätigkeitsmuster-Datenbanken** (activity pattern databases):  Hierin wird die Ausführung verschiedener Tätigkeiten beschrieben inklusive welche Mittel hierfür verwendet werden können und welche Mittel dadurch entstehen bzw. was dadurch erhalten wird bzw. welche Bedürfnisse hierdurch befriedigt werden etc. pp. Die einfachse Form einer Tätigkeitsmuster-Datenbank ist ein Rezeptbuch. Streng genommen sind Tätigkeitsmuster-Datenbanken ebenfalls Mitteldatenbanken, haben allerdings in unserer Systematik eine andere Funktion.
3. **Reputations-Datenbanken** (reputation databases): Hierin wird Usern/Beteiligten Reputation in irgendeiner Form zugeschrieben. Das können etwa Punkte auf stack-exchange sein, Sterne auf einer Markt-Plattform, Würdigungn im lokalen Ehrenamtsverein insofern diese digital auslesbar sind, etc. pp.
4. **Absprachen-Datenbanken** (agreement databases): Ein wesentlicher Teil des Gemeinschaffens sind Absprachen über die Verwendung von Mitteln. In Absprachen-Datenbanken werden solche Absprachen festgehalten und sollten möglichst für die Software verarbeitbar sein. Diese Datenbank ist eng mit Mittel-Datenbanken verknüpft.

Als Projekt wollen wir besonders auf bestehenden Datenbanken aufbauen, aber auch die Entstehung von Datenbanken unterstützen, die unsere Infrastruktur fördern.

## Extractor/Adapter
*(extractor/adapter)*

E/As sind Software-Bausteine, durch welche die verteilten, möglicherweise in verschiedenen Sprachen geschriebenen Datenbanken "auf Linie" gebracht werden, um sie entsprechend gleichermaßen verarbeitbar zu machen. E/As nehmen dabei nicht nur Informationen auf, sondern geben diese an die entsprechenden Datenbanken wieder zurück.  

## Probleme: Bedürfnisse / Bedarfe / problematische Mittelzustände
*(needs / demands / problematic condition of means)*

Diese drei Formen von Problemen stoßen die *Engine* an und es werden entsprechend Tätigkeiten zu ihrer Lösung vorgeschlagen. Bedürfnisse werden dabei von den Beteiligten selbst vermittelt, Bedarfe entstehen je nachdem, welchen Tätigkeiten sich zugeordnet wird und dass ein Mittelzustand problematisch ist, wird erst durch den Abgleich des gegenwärtigen Zustands eines Mittels mit dem durch Absprachen definierten Zustand des Mittels ersichtlich und hierüber vermittelt.

## Engine

Die Engine ist in erster Linie der im [Konfigurationsprozess](/docs/konfigurationsprozess) beschriebene Algorithmus, durch welchen effiziente Tätigkeiten zur Problemlösung im jeweiligen lokalen Kontext vorgeschlagen werden. Die Engine arbeitet mit Informationen der Extraktoren/Adaptern und den drei Problemen, welche an die Engine herangetragen werden. Das Resultat der Engine sind *vorgeschlagene Tätigkeiten* (proposed activities).

## Vorgeschlagene Tätigkeiten

*(proposed activities)*

Eine festgelegte Sammlung von durch die Engine vorgeschlagener Tätigkeiten. Diese Tätigkeiten werden den Beteiligten anhand derer Fähigkeiten, Interessen, Prioritäten, aber auch [Verfügungsmöglichkeiten über Mittel](/docs/momente-des-gesamtaufwandes) nahe gelegt. Je nachdem, was Beteiligte *berücksichtigen* werden ihnen Tätigkeiten entsprechend markiert, welche diesen Berücksichtigungen entsprechen. Jede vorgeschlagene Tätigkeit bezieht sich auf ein Tätigkeitsmuster der entsprechenden Datenbank. Beteiligte können sich Tätigkeiten zuordnen, wodurch die entsprechende Tätigkeit in die *Konfiguration* aufgenommen und später innerhalb einer realen *Kooperation* mit anderen ausgeführt werden kann.

## Konfigurationen

*(configurations)*

Der Zusammenhang von Tätigkeitsmustern und wie durch diese letztendlich das vermittelte Bedürfnis befriedigt und problematische Nebeneffekte der Tätigkeiten wieder aufgehoben sind. Konfigurationen sind eine transparente Auswahl und Anordnungen von Tätigkeitsmustern aus den entsprechenden Datenbanken.

## User-ID

Kommt zugunsten der Übersicht zweimal in der Grafik vor. User können Bedürfnisse kommunizieren und sich bei Absprachen über die Verwendung von Mitteln und den Aufbau von Konfigurationen beteiligten (oben). Und im User-Profil sind dessen Lebensumstände, sozialen Beziehungen und Reputationen abgelegt (unten), nach welchen sich andere Beteiligte wiederum orientieren können. Beteiligte können sich außerdem vorgeschlagenen Tätigkeiten annehmen.

## User-Interfaces

In verschiedenen User-Interfaces können verschiedene Prozess-Informationen angezeigt werden. Zum Beispiel vorgeschlagene Tätigkeiten oder bestehende Konfigurationen. Oder die Kommunikation mit anderen Beteiligten oder die Informationen über die Absprachen über Mittel. Verschiedenste User-Interfaces sind denkbar und auch verschiedene Zwecke, Prozessinformationen darzustellen und sich entsprechend einbringen zu können.
