---
sidebar_position: 1
slug: /essays-uebersicht-und-anhang
---

Der Essay 'der Ausdehnungsdrang moderner Commons', in welchem sich an ['Kapitalismus aufheben'](https://commonism.us) von Meretz/Sutterlütti abgearbeitet wird, war der Auslöser für das Projekt 'Global Commoning System'. Der 'Timeless Way of Re-Production' entstand im ersten Projektjahr und ist eine Interpretation von Christopher Alexanders ['The Timeless Way of Building'](https://en.wikipedia.org/wiki/The_Timeless_Way_of_Building). Im *Timeless Way* wurde die Grundstruktur für die Systematik der Software gelegt.


## Der Ausdehnungsdrang moderner Commons

*Übersicht*

1. **Ein endgültiger Bruch mit sämtlichen Formen der bestehenden Commons:** Wie alles Progressive mit der Zeit reaktionär wird, verhält es sich auch mit der Vorstellung der Commons. Die Idee der Inklusion muss aus dem Bestehenden weitergeführt, aber Commons selbst dafür aus der Utopie heraus neu gedacht werden.

2. **Keimform und Vermittlung:** Die Keimform einer commonistischen Gesellschaft kann nicht, wie die Keimform des Kapitalismus, aus interpersonalen Handlungen heraus entstehen und von dort aus Transpersonal werden. Die interpersonale Ebene muss bewusst um eine transpersonale Funktion erweitert werden, welche Selbstorganisation und die allgemeingültige Zwecksetzung der Mittel außerhalb des eigenen Nahumfelds ermöglicht.

3. **Die Phasen der kapitalistischen Produktion:** Die Produktion nach Bedürfnissen ist eine Produktion, die keinen Wert hervorbringt. Um folgend die Produktion nach Bedürfnis und Produktion nach Verwertung aus individueller Perspektive vergleichen zu können, muss die allgemeine Formel der kapitalistischen Produktion und Zirkulation und ihre Verbindung zur Wertsphäre wiederholt werden.

4. **Situation der Lohnabhängigen:** Obwohl Klassen nicht der Ursprung der kapitalistischen Produktion sind, ist es für die Transformation von grundlegender Bedeutung zu verstehen, dass die abstrakte Arbeit im Kapitalismus für die Lohnabhängigen endlos ist und eine Veränderung der Produktionsweise einen massiven Einbruch des notwendigen Arbeitsvolumens mit sich bringen kann.

5. **Ausdehnung und Aufhebung der kapitalistischen Produktion:** Die Bewegung des Kapitals und die von Marx benannten Qualitäten, welche den Umbruch zu einer fortschrittlichen Produktionsweise notwendig machen.

6. **Die Commons-Struktur:** Mit Hilfe der Netzwerktheorie und folgend einer technisch entwickelten, gemeinsamen Instanz zur Selbstorganisation und Zwecksetzung der gesellschaftlichen Mittel, werden Commons und Commoning aus der Perspektive der Utopie neu gedacht.

7. **Produktion und Distribution innerhalb der Commons-Struktur:** Durch den Prozess des Commonings werden in der Commons-Struktur anstehende Bedürfnisse auf Basis von Freiwilligkeit und kollektiver Verfügung befriedigt. Aus der Grundbewegung „B- – ...c... – B+“ heraus wird eine allgemeine Formel des Commonings entwickelt und eine Folgende, falls die notwendigen gesellschaftlichen Produktions- und Lebensmittel nicht verfügbar sind.

8. **Befriedigung innerhalb der sinnlich-vitalen (1.) und produktiven (2.) Bedürfnisdimension:** Kapitalistische Produktion und Commoning werden miteinander verglichen, wie durch das eine bzw. das andere jeweils die eigenen Bedürfnisse befriedigt werden können.

9. **Bedürfnispriorisierung:** Damit Commoning überhaupt Wurzeln schlagen kann, müssen die sinnlich-vitalen Bedürfnisse derjenigen priorisiert werden, welche selbst für die Befriedigung sinnlich-vitaler Bedürfnisse anderer tätig sind. Der Aspekt der Selbstauswahl darf dabei nicht angetastet werden und Freiwilligkeit selbst soll sich durch die Priorisierung erst entfalten können.

10. **Interpersonales und transpersonales Commoning als parallele Funktionen:** Anhand des Fünfschritts von Klaus Holzkamp werden Vorbedingungen, Entwicklungswidersprüche und Funktionswechsel des interpersonalen Commonings mit dem Transpersonalen verglichen. Die beiden sich ergänzenden Funktionen sollen dabei den Rahmen einer Inklusionsgesellschaft bilden.

    **Dominanzwechsel 1. Effizienz des Commonings:** Warum die Effizienz der kapitalistischen Produktionsweise nicht mit der commonistischen verglichen werden kann und welche Auswirkungen das auf Verwendung der Produktionsmittel hat. Warum eine kapitalistische Struktur nicht übernommen werden kann.

    **Dominanzwechsel 2. Ausdehnung der Commons-Struktur:** Die Commons-Struktur dehnt sich aus durch die zur Befriedigung der sinnlich-vitalen Bedürfnisse notwendige, gesellschaftlichen Produktions- und Lebensmittel. Warum sich die Bedingungen der Lohnarbeit mit einer Ausdehnung der Commons-Struktur nicht verbessern, das kapitalistische System sich aber zunehmend auf diese stützt.

    **Dominanzwechsel 3. Politisches Commoning:** Weil es auf Ebene der Bedürfnisbefriedigung keinen Unterschied macht, ob das Mittel zur sinnlich-vitalen Bedürfnisbefriedigung produziert oder überführt (Enteignung auf Wertebene und Zwecksetzung zur Bedürfnisbefriedigung) wird, kann Tätigkeit im (staats-) politischen Feld selbst als Commoningprozess gedacht werden.

    **Dominanzwechsel 4. Krisendynamik zwischen kapitalistischer Produktion und transpersonalen Commoning:** Grundsätzliches zur kapitalistischen und commonistischen Krise. Die Dynamik zwischen Arbeitenden und Arbeitslosen. Warum die Abhängigkeit von Arbeitslosigkeit des kapitalistischen Systems einen Umbruch zur commonistischen Gesellschaft ermöglicht.


## The Timeless Way of Re-Production

*Anhang*

Ein Letztes: Ich habe bisher offen gelassen, warum die Interpretation von „The Timeless Way of Building“ auf das Commoning bzw. eine Software, welche Tätigkeit im gesellschaftlichen Rahmen nach Commons-Prinzipien vermittelt, meiner Ansicht nach sinnvoll funktioniert. Ich will dabei voranstellen, dass es sich mir selbst erst in der näheren Ausarbeitung des Textes vollständig erschlossen hat, wenn es sich auch von Anfang an sinnvoll *anfühlte* . Das Nachfolgende ist daher eher ein Erklärungsversuch, als eine von Beginn an feststehende Methode. Der Grund, warum ich die Methode überhaupt noch angebe ist rein pragmatisch: Vielleicht habe ich Fehler gemacht. Und falls dem so ist, kann über die Angabe der Methode möglicherweise herausgestellt werden, was ich falsch gedacht habe bzw. kann sie dabei helfen, die Interpretation leichter zu verstehen und gegebenenfalls manches nachzubessern oder zu verändern.

Für mich ist die große Gemeinsamkeit bei Alexander und dem Commoning ein im Zentrum stehender *problematischer innerer Zustand,* welcher nur durch eine *Veränderung der äußeren Welt* gelöst werden kann. Diese Veränderung der äußeren Welt, sprich: die Problemlösung, kann durch Alexanders Methode *in Einzelteile zerlegt* werden. Jedes dieser einzelnen Teile kann *für sich* *stehen* und als *Muster* beschrieben werden. In Bezug auf den jeweiligen Gegenstand kann einem solchen Muster eine *bestimmte Qualität* zur Problemlösung von *allgemeiner Gültigkeit* innerhalb *eines bestimmten* *Kontextes* zugeschrieben werden. Weiter kann ein solches Muster, unabhängig von seiner Qualität, Teil *verschiedener Problemlösungen* sein. Und noch weiter und zu Ende gedacht, kann *die Lösung* *sämtlicher* *wiederkehrender* *Probleme* (im Rahmen des jeweiligen Gegenstandes) durch eine Kombination solcher Muster beschrieben werden. Diese Muster dienen damit einerseits als Wissensspeicher und andererseits auch als Kommunikationsmittel. Und wieder explizit auf das Commoning bezogen, ermöglichen diese beiden Aspekte erst, dass die *Organisation der Tätigkeiten* *zur* *gemeinsamen Veränderung der Welt* auf Augenhöhe funktionieren kann.

Deswegen, ich wiederhole mich, empfinde ich die Software als so grundlegend notwendig um bestehende Herrschaftsverhältnisse auf emanzipatorische Weise überwinden zu können. Weitere sich aus der Struktur heraus ergebende Momente, wie etwa die *Bewegungstendenz* zu *immer* *weniger, aber* *hochwertigeren Mustern,* welche im Commoning einhergeht mit einer geringer werdenden Zahl insgesamt verwendeter Mittel, sprich: *Modularität* wie sie auch von der „Open Source Ecology“ angestrebt wird, legen mir nahe, dass die Interpretation sinnvoll war. Der Text unterliegt dabei einer Creative-Commons-Lizenz (CC BY-NC-SA) und darf damit ohne weitere Nachfrage mit Namensnennung verbreitet und verändert werden, so lange der Text bzw. das daraus hervorgehende Werk nicht kommerziell verwendet wird und denselben Bedingungen unterliegt.
