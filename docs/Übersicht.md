---
sidebar_position: 1
slug: /uebersicht
---


Das **Global Commoning System** (GCS) zeichnet sich durch die Vereinigung von politischer Theorie und Praxis aus. In unserer theoretischen Arbeit versuchen wir [*das Gemeinschaffen*](/docs/glossar) (engl.: Commoning) auf gesamtgesellschaftlicher Ebene zu verstehen und wie die Organisation [*des Gemeinsamen*](/docs/glossar) (engl.: Commons) durch Software unterstützt werden kann, damit ein immer freieres Leben für immer mehr Menschen möglich wird. In unserer praktischen Arbeit suchen wir nach Möglichkeiten, dieser Theorie gerecht zu werden.

## Theorie

Die Theorie des 'Global Commoning Systems' ist umfassend, aber - einmal begriffen - zumindest grundsätzlich leicht zu verstehen. Unter dem Punkt [Einführungen](/docs/einfuehrungen-beschreibung) findest du verschiedene Ansätze, in denen wir versucht haben, das das Konzept des GCS zu erklären. Im Essay ["The Timeless Way of Re-Production"](/docs/timeless-way-of-re-production) wurde die Grundlage unserer Software-Struktur gelegt und in der Textreihe  ["The Global Commoning System"](/docs/gcs-konzept-uebersicht) wird diese Systematik konkretisiert. Die Theorie wird in der entsprechenden Kategorie unseres [Forums](https://community.commoningsystem.org/) diskutiert und für unbekannte Begriffe gibt es einen entsprechenden [Glossar](/docs/glossar).

## Praxis

Die Praxis des 'Global Commoning Systems' zielt auf die Entwicklung und Förderung der in der Theorie beschrieben Software bzw. Software-Infrastruktur ab. Selbstverständlich ist für uns dabei die [*Freiheit dieser Software*](https://www.gnu.org/philosophy/free-sw.de.html). Neben unserem [Anliegen](/docs/anliegen) als Projekt-Team findest du im entsprechenden Reiter auch alles darüber, wie wir als [Team kommunizieren](/docs/team-kommunikation) oder wie der [Einstiegsprozess](/docs/einstiegsprozess) aussieht. Entstehung und Gegenwart des Projektes wird außerdem in Form eines [Podcasts](/docs/podcast) dokumentiert. Im Reiter zur Infrastruktur findest du [Beteiligungsmöglichkeiten](/docs/beteiligungsmoeglichkeiten) im Projekt und auch die bisher erarbeitete [Datenstruktur](/docs/datenstruktur).

## Vorbedingungen

* Theorie: Die sehr große weltanschauliche Grundlage des Projektes ist die [Wertkritik](https://de.wikipedia.org/wiki/Wertkritik) mit ihrer wesentlichen Aussage, dass es im Kontext von Geld und (Lohn-)Arbeit keinen Ausweg aus den bestehenden gesellschaftlichen Problemen gibt (siehe etwa das [Manifest gegen die Arbeit](/docs/manifest-gegen-die-arbeit)). Transformativ liegt dem Projekt insbesondere die durch Stefan Meretz formulierte [Keimform-Theorie](/docs/keimformtheorie) zugrunde. Im Theorie-Bereich wesentlich sind für uns außerdem die Arbeiten von Silke Helfrich mit David Bollier (etwa ihrer Ausarbeitung zu ['Commons und Sprache'](/docs/commons-und-sprache)) und die Arbeiten von Johannes Euler (etwa seine [Definition von Commons und Commonig](/docs/commoning-definition)).

* Praxis: Ohne der Entwicklung und Verbreitung des Internets wäre unser Projekt nicht denkbar gewesen und ohne den Mühen der [Freien Software-Bewegung](https://de.wikipedia.org/wiki/Freie-Software-Bewegung) wäre unser Projekt nicht umsetzbar. Innerhalb der bestehenden Entwicklungen *Freier und Open Source Software* (FOSS) suchen wir unseren Platz und dokumentieren diese Suche in unserer [Umfeldanalyse](/docs/umfeldanalyse). Bisher scheinen die Projekte [valueflows](https://valueflo.ws/), [bonfire](https://bonfirenetworks.org/) und möglicherweise [holochain](https://holochain.org/) für uns besonders relevant zu sein. Kennst du noch weitere Entwicklungen? Melde dich gerne im entsprechenden [Forum-Thread](https://community.commoningsystem.org/t/related-or-similar-developments-and-projects-was-andere-entwicklungen/104/21).

Als Projekt suchen wir stetig nach neuen Aktivist:innen und der Kooperation mit anderen Projekten. Nehmt sehr gerne [Kontakt](/docs/kontakt) mit uns auf.
