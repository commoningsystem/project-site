---
sidebar_position: 11
slug: /danksagungen
---

*Aufgezählt sind folgend Personen/Insitutionen, welche etwa Bilder oder Code unter einer Freien Lizenz zur Verfügung gestellt und auf welche wir zurückgegriffen haben.*



## Forum-Icons:

*Sämtliche Icons wurden dem [noun-project](https://thenounproject.com) entnommen und stehen unter einer Creative Commons - Namensnennung - 4.0 Lizenz*.

*coding* von mim studio, ID
*Tickets* von MRFA, US
*Network* von Marie Van den Broeck, BE
*Text* von Clea Doltz
*unstructured data* von sachin modgekar, IN
*feedback* von Binpodo, ID
*community* von ProSymbols, US


## Icons aus der Textreihe 'Global Commoning System':

*Sämtliche Icons wurden dem [noun-project](https://thenounproject.com) entnommen und stehen unter einer Creative Commons - Namensnennung - 4.0 Lizenz*.

*Linen* von Rineesh
*wooden frame* von Tomek Woloszyn, PL
*alarm* von Larea
*Thread* von Anton
*weave* von Claire Jones
*yarn* von Janira Keana
*spinning Wheel* von Jessie White.
*Nails* von sarah
*hammer* von Atif Arshad
*vice* von AW
*paint* von Justicon
*brush* von Mani Cheng
*yarn* von Janira Keana
*miter saw* von Krista Glanville
*workbench* von Ralf Schmitzer
*Paper knife* von Martin
*painting* von Liv Iko
*wood* von Tanuj Abraham
*hourglass* von Sma-rtez
*no go* von Xela Ub
*Repair* von Adrien Coquet
*give* von Adrien Coquet, FR
*Share* von Aya Sofya
*Skirt* von Made von Made, AU
*room* von Batibull
*Tree* von Guilherme Furtado
*car* von Kero
*world* von Guilherme Furtado
*Fuel* von IconMark
*Skills sharing* von Tomas Knopp
*Chain* von David


## Frontpage Projekt-Seite

Es wurden modifizierte Versionen (Text hinzugefügt) aus dem ['Musters des Commonings'](https://commons.blog/2020/12/11/jetzt-bestellbar-commoning-oder-wie-transformation-gelingt-eine-mustersprache/)-Kartenset verwendet. Deren Urheberin ist Mercè M. Tarrés und die Illustrationen stehen unter dem Copyfarleft des Guerrilla Media Collective unter Nutzung der https://wiki.p2pfoundation.net/Peer_Production_Licence

Verwendet wurden: *Auf gemeinschaftsgetragene Infrastrukturen setzen*, *sich in Vielfalt gemeinsam ausrichten*, *Gemeinsam erzeugen & nutzen* und *Geldunabhängige Sicherheit schaffen*.


## Website

Die Website wurde erstellt mit dem Open-Source-Tool [Docusaurus](https://docusaurus.io/). Docusaurs steht unter der MIT Lizenz. Urheber ist 'Facebook, Inc. and its affiliates'.
