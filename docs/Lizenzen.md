---
sidebar_position: 13
slug: /Lizenzen
---

Soweit nicht anders angegeben stehen alle Texte, welche im Rahmen des *Global Commoning Systems* publiziert werden, unter einer *Creative Commons - Namensnennung - Teilen unter gleichen Bedingungen 4.0+*-Lizenz (CC BY-SA 4.0+).

Soweit nicht anders angegeben, steht jeder im Rahmen des 'Global Commoning Systems' publizierter Code unter einer *GNU Affero General Public License 3+*-Lizenz (AGPLv3+).
