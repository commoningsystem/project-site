---
sidebar_position: 14
slug: /kontakt
---

Unsere Mailadresse für sämtliche Anliegen ist: mail [at] commoningsystem.org

Willst du dich bzw. dein Projekt bei uns vorstellen, kannst du auch unseren [Welcome-Thread von Discourse](https://community.commoningsystem.org/t/willkommen/89) oder den [Welcome-Kanal von Matrix](#gcs-welcome:systemausfall.org) nutzen. Falls du als Aktivist:in bei uns einsteigen willst, schau dir auch gerne unseren [Einstiegsprozess](/docs/einstiegsprozess) an.
