---
sidebar_position: 4
slug: /issmit-app
---

[![Issmit-Animationsfilm](/img/Issmit-Pizza-play.png)](https://tube.tchncs.de/videos/watch/05c4b4b0-d134-45ff-9035-728082c5bb5f)

## Was ist die Issmit-App?

Die issmit.app: Deine Nachbarschaft kocht neuerdings füreinander!

Stell dir vor: Du kannst in einer neuen kostenlosen App eintragen, dass du an deinen Bürotagen jeweils ein gutes Essen haben möchtest, gekocht von Freiwilligen aus der Nachbarschaft. Im Hintergrund zerlegt die App deinen Wunsch in verschiedene Tätigkeitsschritte und zeigt die nötigen Arbeiten, Transporte, Zutaten, etc. wiederum in der App an. Leute, die die App ebenfalls benutzen, können sich nun für diese und andere Tätigkeiten einschreiben und gemeinsam dazu beitragen, dass die Nachbarschaft füreinander kocht. Je mehr Leute mitmachen, desto schneller wird dein Wunsch nach einem nachbarschaftlichen Essen an deinen Bürotagen erfüllt…

Weshalb dies Menschen tun sollten? Weils erstens Spass macht, anderen eine Freude zu bereiten. Zweitens, weil so neue Bekanntschaften im Quartier entstehen. Und weil drittens Mitmachende in der App Anerkennung für ihre Arbeit erhalten, was wiederum die Wahrscheinlichkeit erhöhen kann, dass ihre Wünsche ebenfalls erhört werden.

Die issmit.app ist eine völlig neue Art, deine Nachbarschaft kennenzulernen, tätig zu sein und zu liebevoll zubereitetem Essen zu kommen.

*(Text aus der [Crowdfunding-Kampagne](/docs/issmit-crowdfunding))*


## Die Issmit-App im Kontext des GCS

Zwar soll das GCS eines Tages in sämtlichen gesellschaftlichen Sphären unterstützend wirken können; dass wir davon aber noch weit entfernt sind, wissen wir. Die *Issmit-App* ist der Versuch, die Software-Infrastruktur auf einen Anwendungsbereich zu beschränken und wir hoffen darauf, dass sich dort eine erste Community findet, mit der wir gemeinsam die Entwicklung voran bringen. Ist die Issmit-App erst einmal stabil, sollen die Nutzenden befragt werden, in welchen weiteren gesellschaftlichen Bereich sie Unterstützung durch dieses Werkzeug benötigen könnten. Das wäre der Start der zweiten App und ein weiterer Schritt, um die durch das 'Global Commoning System' unterstützte Infrastruktur des Gemeinschaffens zu erweitern und zu stärken.



import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Datenstruktur/Issmit-Kontext-light.svg'),
    dark: useBaseUrl('/img/Datenstruktur/Issmit-Kontext-dark.svg'),
  }}
/>


## Crowdfunding-Grafiken

Für das Crowdfunding im Frühjahr 2021 entstanden mehrere Grafiken:

![CF1](/img/Issmit-crowdfunding/Grafiken_Crowdfunding.jpg)
![CF2](/img/Issmit-crowdfunding/Grafiken_Crowdfunding2.jpg)
![CF3](/img/Issmit-crowdfunding/Grafiken_Crowdfunding3.jpg)
![CF4](/img/Issmit-crowdfunding/Grafiken_Crowdfunding4.jpg)
![CF6](/img/Issmit-crowdfunding/Grafiken_Crowdfunding6.jpg)
![CF5](/img/Issmit-crowdfunding/Grafiken_Crowdfunding5.jpg)
