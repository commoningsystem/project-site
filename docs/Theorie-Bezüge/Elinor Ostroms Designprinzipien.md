---
sidebar_position: 2
slug: /designprinzipien
---

*Folgend direkt zitiert aus Helfrich/Bollier, ["Frei, Fair und Lebendig"](/docs/theorie-distro), S.317*


Diese Prinzipien hat Elinor Ostrom bereits 1990 in ihrem Hauptwerk, Governing
the Commons (Die Verfassung der Allmende), veröffentlicht. In ihrer Nobelpreis-
rede 2009 stellte sie eine gemeinsam mit Michael Cox, Gwen Arnold und Sergio
Villamayor-Tomás präzisierte Fassung vor, die hier stichpunktartig wiedergegeben
wird:

1. **Grenzen:** Es existieren klare und lokal akzeptierte Grenzen zwischen legitimen Nutzern und Nicht-Nutzungsberechtigten. Es existieren klare Grenzen zwischen einem spezifischen Gemeinressourcensystem und einem größeren sozioökologischen System.

2. **Kongruenz:** Die Regeln für die Aneignung und Reproduktion einer Ressource entsprechen den örtlichen und den kulturellen Bedingungen. Aneignungs- und Bereitstellungsregeln sind aufeinander abgestimmt; die Verteilung der Kosten unter den Nutzern ist proportional zur Verteilung des Nutzens.

3. **Gemeinschaftliche Entscheidungen:** Die meisten Personen, die von einem Ressourcensystem betroffen sind, können an Entscheidungen zur Bestimmung und Änderung der Nutzungsregeln teilnehmen (auch wenn viele diese Möglichkeit nicht wahrnehmen).

4. **Monitoring der Nutzer und der Ressource:** Es muss ausreichend Kontrolle über Ressourcen geben, um Regelverstößen vorbeugen zu können. Personen, die mit der Überwachung der Ressource und deren Aneignung betraut sind, müssen selbst Nutzer oder den Nutzern rechenschaftspflichtig sein.

5. **Abgestufte Sanktionen:** Verhängte Sanktionen sollen in einem vernünftigen Verhältnis zum verursachten Problem stehen. Die Bestrafung von Regelverletzungen beginnt auf niedrigem Niveau und verschärft sich, wenn Nutzer eine Regel mehrfach verletzen.

6. **Konfliktlösungsmechanismen:** Konfliktlösungsmechanismen müssen schnell, günstig und direkt sein. Es gibt lokale Räume für die Lösung von Konflikten zwischen Nutzern sowie zwischen Nutzern und Behörden [z.B. Mediation].

7. **Anerkennung:** Es ist ein Mindestmaß staatlicher Anerkennung des Rechtes der Nutzer erforderlich, ihre eigenen Regeln zu bestimmen.

8. **Eingebettete Institutionen (für große Ressourcensysteme):** Wenn eine Gemeinressource eng mit einem großen Ressourcensystem verbunden ist, sind Governance-Strukturen auf mehreren Ebenen miteinander »verschachtelt« (Polyzentrische Governance).


Literatur: Elinor Ostrom: »Beyond Markets and States: Polycentric Governance of Complex Economic Systems«, Nobelpreisrede vom 8. Dezember 2009, online verfügbar unter: www.nobelprize.org/nobel_prizes/economics/laureates/2009/ostrom-lecture.html.
