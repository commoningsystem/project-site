---
sidebar_position: 6
slug: /theorie-distro
---

Bei unserer Crowdfunding-Kampagne für die [Issmit-App](/docs/issmit-app) haben wir als Dankeschön eine Zusammenstellung von pdfs zur Theorie und Praxis von Commoning angeboten. Sämtliche Werke stehen dabei unter Creative-Commons-Lizenzen bzw. der Open-Public-License und können - im Rahmen der jeweiligen [Lizenz-Gestaltung](https://creativecommons.org/licenses/?lang=de) - frei verbreitet werden. In der Tradition der [Linux-Distributionen](https://de.wikipedia.org/wiki/Linux-Distribution) wollen wir diese Zusammenstellung natürlich ebenso frei und ohne verpflichtende Spende zur Verfügung stellen:

*
<a
  target="_blank"
  href={require('/pdf/div/Johannes Euler - Wasser als Gemeinsames.pdf').default}>
  Euler, Johannes - Wasser als Gemeinsames (CC BY)
</a>

*
<a
  target="_blank"
  href={require('/pdf/div/Friederike Habermann - Ausgetauscht.pdf').default}>
  Habermann, Friederike - Ausgetauscht (CC BY-NC-SA)
</a>

*
<a
  target="_blank"
  href={require('/pdf/div/Helfrich&Bollier - Frei, Fair und Lebendig.pdf').default}>
  Helfrich/Bollier - Fair, Frei und Lebendig (CC BY-SA)
</a>

*
<a
  target="_blank"
  href={require('/pdf/div/Till Kreutzer - Open Content.pdf').default}>
  Kreutzer, Till - Open Content (CC BY)
</a>

*
<a
  target="_blank"
  href={require('/pdf/div/Eric Raymond - the cathedral and the bazaar.pdf').default}>
  Raymond, Eric - The Cathedral and the Bazar (OPLv2)
</a>

*
<a
  target="_blank"
  href={require('/pdf/div/Sutterluetti&Meretz - Kapitalismus aufheben.pdf').default}>
  Sutterlütti/Meretz - Kapitalismus aufheben (CC BY-NC-ND)
</a>

*
<a
  target="_blank"
  href={require('/pdf/div/Wissen wuchern lassen.pdf').default}>
  Wissen wuchern lassen (CC BY-SA)
</a>
