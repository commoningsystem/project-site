---
sidebar_position: 3
slug: /beteiligungsmoeglichkeiten
---

Es gibt viele Bereiche, in welchen sich in das 'Global Commoning System' eingebracht werden kann und in jedem Bereich wird Unterstützung benötigt. Zur Übersicht haben wir die Bereiche in sechs Teile gegliedert:

1. **eingeschlossene Software-Komponenten:** Alles was speziell für unser Projekt entwickelt werden muss, aber außerhalb des Projektes vermutlich nicht weiterverwendet werden kann.

2. **eigenständig nötige Software-Komponenten:** Alles was entwickelt werden muss, aber auch unabhängig von unserem Projekt von anderen genutzt werden kann. Wir hoffen auch darauf, dass diese Software zumindest in Teilen schon existiert und wir diese verwenden bzw. darauf aufbauen können.

3. **eingeschlossene Themengebiete:** Alles was, von der Software-Entwicklung abgesehen, speziell für dieses Projekt gemacht werden muss und außerhalb unseres Projektes nur schwer nutzbar sein wird.

4. **eigenständig nötige Themengebiete:** Alles was, von der Software-Entwicklung abgesehen, zur Funktionalität unseres Projektes getan werden muss, aber auch außerhalb unseres Projektes von Nutzen sein kann.

5. **Reichweite:** Alles was getan werden muss, um mit dem Projekt Menschen zu erreichen und anzusprechen.

6. **Beständigkeit:** Alles was getan werden muss, damit das Projekt auf Dauer existieren kann.

Die folgende Grafik kann bei der Übersicht helfen, wobei kein Anspruch auf Vollständigkeit der Unterpunkte besteht:

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Beteiligungsmöglichkeiten/beteiligung_light.svg'),
    dark: useBaseUrl('/img/Beteiligungsmöglichkeiten/beteiligung_dark.svg'),
  }}
/>



Falls du dich einbringen möchtest, [melde dich gerne bei uns](/docs/kontakt)!
