---
sidebar_position: 4
slug: /einstiegsprozess
---

Wir haben weit mehr zu tun, als wir als Projekt-Team gerade leisten können und sind uns der meisten Arbeit wohl noch nicht einmal bewusst. Falls du dich daher einbringen willst: Fühl dich sehr willkommen!

Wir sind ein Freies Software-Projekt und schaffen es schon seit einiger Zeit kontinuierlich aktiv zu sein. Mittlerweile haben wir einen kleinen Einstiegsprozess erarbeitet, der dir hoffentlich hilft Teil des Projektes zu werden und gleichzeitig dafür sorgt, dass wir uns als Team untereinander wohlfühlen.

1. Das Team definiert sich zur Zeit hauptsächlich durch die Teilnahme an den **Koordinations-Meetings**. Entweder du wirst dazu eingeladen oder du fragst an, eingeladen zu werden - wichtig ist nur, dass du nicht einfach spontan dazu kommst, da es für alle Beteiligten wohl eher irritierend ist.

2. Vor deiner Teilnahme am Koordinations-Meeting gibt es ein **Vorgespräch** mit einer oder mehreren Personen, welche in dem Bereich tätig sind, in dem du dich vorrangig auch einbringen willst. Wenn du keinen speziellen Bereich wie etwa Design, Theorie oder Entwicklung hast, ist das natürlich überhaupt kein Problem. Im Vorgespräch werden deine ersten offenen Fragen zum Projekt beantwortet und es ist natürlich auch ein Raum, um über deine Motivation und Vorerfahrungen zu sprechen.

3. Diejenigen, die mit dir das Vorgespräch geführt haben, geben im nächsten Koordinations-Meeting an das Team weiter, wer du bist und was dich zum Projekt geführt hat. Dann kommst du zum Meeting dazu, das Team stellt sich vor und wieder können Fragen von beiden Seiten gestellt werden. Nach dieser **Vorstellung** beginnt das reguläre Meeting bzw. geht das Meeting wie gewohnt weiter. Bis das Team ein Abklärungs-Treffen abgehalten hat, bist du herzlich eingeladen, an den Meetings teilzunehmen.

4. Es gibt eine **Begleitperson** für Neuzugänge. Die Person ist in der Regel einfach ein Team-Mitglied, das zu dieser Zeit Lust und Kapazität dafür hat. Natürlich kannst du immer und jederzeit Fragen stellen, aber wenn du Unsicherheiten hast oder vermeintlich dumme Fragen nicht vor ganzer Runde stellen willst, ist sie dein Ansprechpartner. Eine Aufgabe dieser Person ist es auch, dich gelegentlich zu fragen, wie der Einstiegsprozess für dich läuft. Falls du dich mit dieser Begleitperson nicht wohl fühlen solltest, kannst du das natürlich gerne gegenüber einer Person deiner Wahl äußern.

5. Nach etwa 3-5 Meetings mit dir hält das alte Team ein kurzes **Abklärungs-Treffen** ab, das von der Begleitperson einberufen wird. Abgeklärt wird, ob es Bedenken von Team-Mitgliedern gibt, dass die Zusammenarbeit mit dir vielleicht nicht in die richtige Richtung geht. Das Treffen ist insofern wichtig, da bei einem so offenem und auf freiwilliger Tätigkeit beruhendem Projekt wie unserem sichergestellt werden muss, dass jede Person sich u.a. in diesen Koordinations-Meetings auch wohl fühlt. So ein Treffen ist übrigens obligatorisch - nur also, weil eines stattfindet, heißt das noch lange nicht, dass jemand Vorbehalte gegen dich hat.

6. Wenn dieses Abklärungs-Treffen stattgefunden hat und niemand bei dir größere Bedenken hat: **Willkommen im Team!** Wir freuen uns sehr auf dich!
