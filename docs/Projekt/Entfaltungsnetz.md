---
sidebar_position: 6
slug: /entfaltungsnetz
---

Bisher sind wir als Aktivist:innen weitgehend unentgeltlich tätig, erhoffen uns aber zukünftig für unsere Tätigkeit hier von der Lohnarbeit befreit zu sein. Zur Zeit bereiten wir uns darauf vor, dass eines Tages durch Crowdfunding oder Förderungen Geld fließt und ein Teil davon für unseren Lebensunterhalt verwendet werden kann. Wie das dann aber schließlich unter uns fair und bedürfnisorientiert verteilt werden kann, ist eine große Frage.

Eine Möglichkeit ist das [Entfaltungsnetz](https://extinctionrebellion.de/spenden/entfaltungsnetz/), welches im Rahmen von [Extinction Rebellion](https://extinctionrebellion.de/) entstand, darüber hinaus aber auch bei anderen Projekten Verwendung finden soll. Einige Aktivist:innen unseres Projekt probieren gerade aus, ob es für unser Projekt passend ist und uns auch langfristig hilft. Auf [matrix](/docs/team-kommunikation) gibt es auch einen entsprechenden Chat-Raum hierfür, zu dem du gerne eingeladen werden kannst.

Bis dahin ist es auch möglich, als Aktivist:in a) eine Rechnung zu stellen oder b) Geld zu beantragen, falls es in der jeweiligen Lebenssituation gerade notwendig ist. Die Anträge können von anderen Team-Mitgliedern diskutiert werden und bewegen sich selbstverständlich innerhalb unseres bisher recht schmalen Budgets.
