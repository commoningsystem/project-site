---
sidebar_position: 2
---

### Vorgehen

Das 'Global Commoning System' vereint Theorie und Praxis. In unserer theoretischen Arbeit versuchen wir *das Gemeinschaffen* (engl.: Commoning) auf gesamtgesellschaftlicher Ebene zu verstehen und wie die Organisation *des Gemeinsamen* (engl.: Commons) unterstützt werden kann, damit ein immer freieres Leben für immer mehr Menschen möglich wird. In unserer praktischen Arbeit suchen wir nach Möglichkeiten, dieser Theorie gerecht zu werden. Wir programmieren hierfür neue Software, agieren als Schnittstelle von bestehenden Projekten und streben zielgerichtete Kooperationen an. 

[Freie/Libre und](https://de.wikipedia.org/wiki/Freie_Software) [Open Source Software](https://de.wikipedia.org/wiki/Open_Source	) (F/LOSS) ermöglicht es uns, auf den Arbeiten anderer aufzubauen und selbst Trittbrett neuerer Entwicklungen zu werden. Die Lizenzierung unserer Software als Freie Software (AGPLv3+) und unser Texte als Creative Commons (CC BY-SA 4.0+, falls nicht anders angegeben) ist für uns selbstverständlich.

Das Projekt 'Global Commoning System' ist umfassend, aber - einmal begriffen - zumindest konzeptionell leicht zu verstehen. Unter dem Punkt [Einführungen] findest du verschiedene Ansätze, in denen wir versucht haben, das GCS zu erklären. Detailliert wird die Methode in der gleichnamigen Textreihe ["The Global Commoning System"] beschrieben. 

Zwar soll das GCS eines Tages in sämtlichen gesellschaftlichen Sphären unterstützend wirken können; dass wir davon aber noch weit entfernt sind, wissen wir. Die **Issmit-App** ist der Versuch, die Software auf einen Anwendungsbereich zu beschränken und wir hoffen darauf, dass sich dort eine erste Community findet, mit der wir gemeinsam die Entwicklung voran bringen. Ist die Issmit-App erst einmal stabil, sollen die Nutzenden befragt werden, in welchen weiteren gesellschaftlichen Bereich sie Unterstützung durch dieses Werkzeug benötigen könnten. Das wäre der Start der zweiten App und ein weiterer Schritt, um die durch das 'Global Commoning System' unterstützte Infrastruktur des Gemeinschaffens zu erweitern und zu stärken. 