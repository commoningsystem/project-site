---
sidebar_position: 7
slug: /podcast
---

Als 'Global Commoning System' wollen wir uns Entstehung und Gegenwart des Projektes durch Aktivist:innen im Gespräch festhalten. Der Podcast ist auf der funkwhale-Instanz [open.audio](https://open.audio/) zu finden bzw. führt dich der Link auf dem nachfolgenden Bild zu ihm:

[![GCS-Podcast](/img/Podcast_link.jpg)](https://open.audio/channels/gcs_werkstattbericht/)
