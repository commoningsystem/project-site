---
sidebar_position: 1
slug: /anliegen
---

Der Zweck des 'Global Commoning Systems' ist die Konstruktion und Etablierung einer neuen gesellschaftlichen Form, wie sich Tätigkeiten zur Befriedigung von Bedürfnissen angenommen werden kann. Diese neue Form baut auf dem *Gemeinsamen* auf, dessen effiziente Organisation zur Bedürfnisbefriedigung unterstützt werden soll.

Kern dieser neuen Form ist die **Selbstorganisation durch Selbstzuordnung**. Dieser Prozess soll von einer Software unterstützt werden, welche sinnvolle Tätigkeiten zur höchstmöglichen Bedürfnisbefriedigung aller Beteiligten auf Grundlage der lokal verfügbaren Mitteln vorschlägt. Durch Selbstzuordnung zu diesen Vorschlägen können auch einander unbekannte Menschen anfangen miteinander zum gemeinsamen Vorteil zu kooperieren oder etwa die Verwendung dieser *gemeinsamen* Mittel zu diskutieren. Was wir entwickeln ist ein Werkzeug, um einerseits Nachbarschaften unterstützen, sich gegenseitig unabhängig von Einkommen oder sozialen Beziehungen zu helfen, anderseits soll dieses Werkzeug im größeren Rahmen eine gesellschaftliche Transformation im Sinne der [Keimform-Theorie](/docs/keimformtheorie) voran bringen.

Eine gesellschaftliche Transformation ist notwendig, um mit der Welt als einer *Gemeinsamen* umgehen zu können und hierdurch Lösungen für die Probleme unserer Zeit zu finden. Bei der von uns angestrebten Transformation sind Weg und Ziel ein und dasselbe; es wird uns nur durch jeden Schritt leichter fallen, auf Augenhöhe füreinander da zu sein und unsere Existenz unabhängig von Markt und Staat gesichert zu wissen. Erreicht werden soll eine Gesellschaft, in der wir uns Aufgaben annehmen können, die wir selbst als sinnvoll für uns und andere erachten, aber genauso ungestraft nichts tun können, wenn es nichts zu tun gibt oder wir nichts tun wollen. Eine Bedingung hierfür ist die Transparenz gesellschaftlicher Bereichen wie der Produktion, der Pflege und dem Transport, um deren Strukturen diskutier- und veränderbar zu machen.

Was wir wollen ist nicht utopisch, sondern seit der Entstehung und Verbreitung des Internets eine realistische Möglichkeit. Eine Möglichkeit, die nicht von selbst entstehen wird, aber die wir mit gemeinsamer Anstrengung erreichen können.
