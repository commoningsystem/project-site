---
sidebar_position: 2
slug: /team-kommunikation
---

Als Aktivist:innen kommunizieren wir auf verschiedenen Kanälen.

Jeden Mittwoch halten wir ein **Koordinations-Meeting** ab. Traktanden (Topics) werden [in einem Pad gesammelt](https://pad.riseup.net/p/Transcomm-keep) und schließlich schrittweise abgearbeitet. Muss ein Traktand länger diskutiert werden, kann er an das Ende des Treffens geschoben werden, damit nicht daran Interessierte (oder Personen mit weniger Zeit) das Meeting verlassen können.

In der letzten Sitzung des Monats treffen wir uns 30 Minuten vor Beginn, für Off-topic und um bisherige Erfolge zu feiern.

In den Koordinations-Meetings ergibt sich häufig der Bedarf nach einem **Workshop**, zu welchen sich schließlich Aktivist:innen in unterschiedlichen Konstellationen treffen, um die jeweiligen Fragestellungen abzuarbeiten. Die Workshops finden oft montags oder nach dem Meeting statt.

Die meisten projektbezogenen Themen werden in unserem **Forum**, eine Discourse-Instanz, beprochen: [community.commoningsystem.org](https://community.commoningsystem.org)

Als **Chat** verwenden wir den dezentralen Messanger [[matrix]](https://matrix.org/). Ähnlicher einer Mail-Adresse brauchst du für Matrix einen Homeserver. [Hier](https://publiclist.anchel.nl/) findest du eine Liste von öffentlichen Servern, auf denen du dich registrieren kannst.

*öffentliche Matrix-Räume*: Hauptraum (commoningsystem:systemausfall.org) ; GCS Welcome (#gcs-welcome:systemausfall.org) ; GCS Off-Topic (#gcs-off-topic:systemausfall.org)

*private Matrix-Räume:* GCS Design, GCS Projektraum, GCS Finanzen, GCS Entfaltungsnetz, GCS Kommunikation

Unsere **Mailadresse** ist: mail@commoningsystem.org
