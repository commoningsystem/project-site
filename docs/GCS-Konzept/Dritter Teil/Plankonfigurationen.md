---
sidebar_position: 5
slug: /plankonfigurationen
---

*Plankonfigurationen* sind besondere Momente* innerhalb des Konfigurationsprozesses*. Als geplante Konfigurationen gelten solche, bei denen der Prozess der *Auswahl und Anordnung* von Tätigkeiten nicht innerhalb, sondern außerhalb der Software-Vermittlung durch konkrete Personen vor sich geht. Diese Person bzw. diese Personen *planen*, wie eine Kooperation zu einem bestimmten Zweck – zum Beispiel der Verfügbarmachung eines Mittels – vonstatten gehen soll und integrieren ihre eigene Vorstellung schließlich in die Softwarestruktur. Im Gegensatz zu →[*integrierten Zusammenschlüssen*], in welchen sich von Tätigkeitsmustern unabhängig in das ununterbrochene Commoning eingebracht wird, wird bei einer Plankonfiguration mit eben diesen Tätigkeitsmustern auf Softwareebene gearbeitet. Auf diese Weise bleibt auch diese Konfiguration – welche Teil größerer Konfigurationen sein kann – für alle Beteiligten transparent. Dagegen ist es auch die Schwierigkeit bei Plankonfigurationen, die Prozesse, die sich außerhalb der Software-Vermittlung wie selbstverständlich vorgestellt werden, in der Sprache von Tätigkeitsmustern auszudrücken.

Ein Zweck der Plankonfiguration kann es sein, der Softwarelogik eigene Vorstellungen entgegenzuhalten. Ein anderer Zweck von Plankonfigurationen kann sein, dass die Konfiguration nicht von Seite der Bedürfnisse, sondern von der Seite der Mittel aus gedacht wird. Also ein: „Wir haben gerade *das und das* zur Verfügung und es wird schlecht/steht im Weg; was können wir damit machen – also welche Bedürfnisse können damit befriedigt werden?“ Nach der Planung einer Konfiguration kann sich den Tätigkeiten entweder selbst zugeordnet werden oder diese werden, wie Tätigkeiten aus dem Konfigurationsprozess, allgemein vorgeschlagen und können in die persönliche Vorauswahl von Beteiligten übernommen werden. Da in Plankonfigurationen vorgeschlagene Tätigkeiten im jeweiligen lokalen Umfeld zumindest aus technischer Hinsicht weniger *ideal* zur allgemeinen Bedürfnisbefriedigung sein können als solche Tätigkeiten, die durch den Konfigurationsprozess vorgeschlagen werden, braucht es für die Beteiligten eine klare Markierung, was der Ursprung des jeweiligen Vorschlags ist. Bei Plankonfigurationen sollte außerdem eine optionale Beschreibung möglich sein, *warum* die Person, welche die Konfiguration geplant hat, diese Auswahl und Anordnung von Tätigkeiten als sinnvoll empfindet.

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/3/light/Konfig_Planungsprozess.svg'),
    dark: useBaseUrl('/img/Textreihe/3/dark/Konfig_Planungsprozess_dark.svg'),
  }}
/>


Die Markierung des Ursprungs ist ebenfalls für den →[*sozialen Prozess*] um die Verwendung von Mitteln notwendig. Es macht einen Unterschied, ob es heißt, dass das sich in der Verwendung aufbrauchende Mittel [*xy*] in einer Plankonfiguration zu *diesem und jenen Zweck* verwendet werden soll oder dasselbe Mittel in einem Konfigurationsprozess zu einem anderen Zweck verplant wurde, der zumindest aus technischer Perspektive daraus ausgelegt ist, möglichst viele Bedürfnisse mit einzubeziehen. Wie immer heißt das aber nicht, dass solche Entscheidungen zur Verwendung bestimmter Mittel unbedingt zugunsten der durch den Konfigurationsprozess vorgeschlagenen Lösungen ausfallen müssen – im sozialen Prozess, also der Entscheidungsfindung von Betroffenen, sind das lediglich Indikatoren.
