---
sidebar_position: 6
slug: /kontinuitaet
---

Eine arbeitsteilige bzw. eine auf *komplexer Kooperation* beruhende Gesellschaft zeichnet sich dadurch aus, dass einzelne Tätigkeiten tendenziell nicht nur auf die Befriedigung eines einzelnen Bedürfnisses abzielen, sondern möglichst viele Bedürfnisse mit einschließen. Würde der Webstuhl in etwa nur für die Produktion von soviel Leinengewebe aufgespannt werden, wie für eine einzelne Leinwand notwendig ist, dann würde sich der dafür aufgebrachte Aufwand in keiner Weise lohnen. Der Aufwand für diese Tätigkeit lohnt sich erst, wenn über die für die einzelne Leinwand notwendige Menge hinaus gewebt, also der Bedarf von noch mehr Bedürfnissen mit einbezogen wird. Ob das Resultat der Konfiguration dann eine Leinwand oder etwa ein Rock ist, ist dabei unerheblich, wenn in der entsprechenden Konfiguration der Bedarf nach Leinengewebe ansteht. Eine Tätigkeit, die über die Befriedigung eines einzelnen Bedürfnisses hinaus geht, wird folgend als *kontinuierliche Tätigkeit* bezeichnet. Innerhalb der Struktur des ununterbrochenen Commonings kann eine Tätigkeit dabei kontinuierlich werden, wenn mehrere Konfigurationen an dieser Stelle miteinander *vereinigt *sind*. Eine Tätigkeit gewinnt dabei an →[*Wichtigkeit*], je mehr Prozesse der Bedürfnisbefriedigung sie ermöglicht.

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/3/light/kleine Vereinigung-zentriert.svg'),
    dark: useBaseUrl('/img/Textreihe/3/dark/kleine Vereinigung-zentriert_dark.svg'),
  }}
/>


## Vereinigung einzelner Konfigurationen

Wie kommt es dazu, dass Konfigurationen miteinander vereinigt werden?

Das wichtigste dabei zuerst: Bedürfnisse sind immer individuell und werden daher auch nie als gebündelt betrachtet, selbst wenn in einer bestimmten lokalen Umgebung dieselben Bedürfnisse anstehen. Jedes Bedürfnis wird einzeln vermittelt (→[*Bedürfnisvermittlung*]), hierdurch wird konkretisiert, welche Mittel zur Befriedigung des Bedürfnisses verfügbar sein müssen und über den →[*Konfigurationsprozess*](/docs/konfigurationsprozess) wird für jeden nicht-verfügbaren Bedarf zur Bedürfnisbefriedigung die Tätigkeit vorgeschlagen, welche nach den der Software bekannten Informationen in der jeweiligen lokalen Umgebung am effizientesten das jeweilige Mittel verfügbar macht. Und angenommen bei all diesen gleichen Bedürfnissen in unmittelbarer lokaler Nähe fehlt es am selben Mittel – was insofern Sinn ergibt, da sie in lokaler Nähe zueinander sind – wird in diesem Fall mit hoher Wahrscheinlichkeit *dieselbe* Tätigkeit zur Verfügbarmachung dieses Mittels vorgeschlagen. Das heißt *dieselbe Tätigkeit* wird von verschiedenen *Positionen* aus vorgeschlagen, die aber in relativer Nähe zueinander stehen. Aber auch jetzt, sind die Konfigurationen noch nicht vereinigt! Die *Vereinigung* der Konfigurationen – und damit auch die darauf folgende *kontinuierliche *Ausführung – geschieht erst durch den Prozess der Selbstzuordnung und ist abhängig von der Lokalität der Person, welche sich in das ‚ununterbrochene Commoning‘ einbringen möchte.

Zur Verdeutlichung: Angenommen wir haben eine sehr lange gerade Straße von 10km in welcher zehnmal das Bedürfnis nach ‚künstlerischer Auslebung‘ (\#kA) vermittelt wurde, welches durch Pinsel, Leinwand und Farbe befriedigt werden kann. Das Bedürfnis ist jeweils im Abstand von etwa einem Kilometer voneinander vermittelt worden, Pinsel und Farbe stehen genügend zur Verfügung und über den Konfigurationsprozess stellt sich heraus, dass die altbekannte Tätigkeit ‚Herstellung einer Leinwand‘ (\#HstLw) im lokalen Umkreis jeweils am effizientesten ist, um die Leinwand verfügbar zu machen. In diesem Moment, in dem sich noch niemand auch nur einem Bedürfnis davon angenommen hat, *haben auch die vorgeschlagenen Tätigkeiten eine bestimmte Lokalität* – und zwar die jeweils selbe, wie das vermittelte Bedürfnis. Und jetzt steht eine Person am Ende dieser Straße, möchte sich in das ‚ununterbrochene Commoning‘ einbringen und durchsucht die lokale Umgebung nach Tätigkeiten, denen sie sich annehmen kann und will, hat allerdings ihre Suche auf nur 7*km* eingeschränkt. Falls ihre Interessen und Fähigkeiten die Tätigkeit der Leinwandherstellung einschließen, bekommt sie die entsprechende Tätigkeit in der ‚persönlichen Vorauswahl‘ angezeigt und dazu ebenfalls, dass dieser Tätigkeit in dem von ihr gewählten Umkreis sieben mal nachgegangen werden kann. Aus Zeitmangel oder weil ihr etwa nur eine bestimmte Menge des dafür notwendigen Bedarfs zur Verfügung steht, entscheidet sie sich dafür, *vier* der notwendigen Leinwände herzustellen. Das heißt, sie ordnet sich der Tätigkeit ‚Herstellung einer Leinwand‘ (\#HstLw) in der Menge 4 zu. Und erst in diesem Moment und durch ihre Person werden die Konfigurationen *vereinigt*, da sie (voraussichtlich) der Tätigkeit *kontinuierlich* nachgehen wird, bis der Bedarf viermal gedeckt wurde. Falls nicht sämtliche für die Tätigkeit notwendigen Mittel vorhanden sind – es etwa wieder an Leinengewebe fehlt – wird dieser Bedarf allerdings nicht viermal vermittelt, sondern ein einziges mal mit der vierfachen Menge von ihrer Position aus.

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/3/light/Kontinuität_Straße.svg'),
    dark: useBaseUrl('/img/Textreihe/3/dark/Kontinuität_Straße_dark.svg'),
  }}
/>

Sich einer bestimmten Menge an *Wiederholungen* zuzuordnen, wird als *begrenzte Selbstzuordnung* bezeichnet. Bei einer *unbegrenzten Selbstzuordnung* dagegen wird sich allen notwendigen Ausführungen derselben vorgeschlagenen Tätigkeit in der gewählten Umgebung zugeordnet und außerdem wird die sich zugeordnete Person damit immer *zuerst* angefragt, wenn die Tätigkeit in einem Konfigurationsprozess wieder vorgeschlagen wird. Unabhängig davon, ob sie sich bisher nur *zugeordnet* hat, ob sie die Tätigkeit bereits *ausführt* oder ob sie sich nach mindestens einer Ausführung dafür *bereithält*. Der Vorteil davon ist ein Moment der *Stabilität*. Die Person kann sich in etwa eine Werkstatt zu diesem Zweck einrichten und auch von der Software unabhängig kann bekannt werden, dass sich *dort* bestimmten Problemen angenommen wird. Die *unbegrenzte Selbstzuordnung* ist damit auch ein wesentlicher Moment zur Entstehung von *→[integrierten Zusammenschlüssen]*, denen sich im Verlauf der Textreihe noch im Detail angenommen wird und die ein Zusammenspiel von *Software-vermittelten und nicht-Software-vermittelten* Commoning ermöglichen.

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/3/light/Kontinuität_Freischaltung.svg'),
    dark: useBaseUrl('/img/Textreihe/3/dark/Kontinuität_Freischaltung_dark.svg'),
  }}
/>



## Auswirkung der Kontinuität auf den Gesamtaufwand

Eine Konfiguration breitet sich in Tiefe und Breite aus, da unterschiedliche Mittel zur Befriedigung des Bedürfnisses benötigt werden. Unterschiedliche Konfigurationen werden zusammengeführt, wenn sich die benötigten Mittel verschiedener Konfigurationen untereinander überschneiden. Im Allgemeinen wird der Aufwand einer bestimmten Tätigkeit dabei höher, je mehr Bedarfen sich mit der eigenen Tätigkeit angenommen wird. Allerdings muss hier wieder unterschieden werden, zwischen Mitteln als Resultat, die *aufgeteilt* werden müssen (also die sich aufbrauchen), die *gemeinsam genutzt *werden können oder die sich *beim Teilen vermehren. *Wie sich je nachdem der Aufwand verändert wird insbesondere im Konfigurationsprozess bedeutend, wenn eben die Tätigkeiten vorgeschlagen werden, welche die geringste spekulative Gesamtdauer nach sich ziehen.

Inwiefern wirkt sich die *Art* des Mittels, welches als Resultat durch die Tätigkeit hervorkommt, auf den Aufwand dieser Tätigkeit bei der Vereinigung von Konfigurationen aus?

1.  *Bei Mitteln, die aufgeteilt werden müssen („verbraucht werden“):* Der Aufwand einer einzigen Ausführung kann schlicht mit der Anzahl an Wiederholungen multipliziert werden

2.  *Bei Mitteln, die gemeinsam genutzt werden können:* Der Aufwand wird ebenfalls schlicht mit der Anzahl an Wiederholungen multipliziert. Allerdings können Absprachen zwischen den Personen, welche das Resultat erhalten, zur gemeinsamen Nutzung getroffen und somit die Anzahl notwendiger Wiederholungen reduziert werden. Da es hier eben der Absprachen zwischen konkreten Personen bedarf, ist es problematisch im Konfigurationsprozess die spekulative Gesamtdauer einer einzelnen Konfiguration zu planen. Es braucht daher die Möglichkeit, dass die bedarfsvermittelnde Person in für die Software auslesbaren Bedingungen angibt, dass das benötigte Mittel auch mit anderen gemeinsam verwendet werden kann. Falls sich dann herausstellt, dass dieses Mittel, das gemeinsam genutzt werden kann, nur ein einziges Mal verfügbar gemacht werden muss um in verschiedenen Konfigurationen verwendet zu werden, dann kann der Aufwand der Tätigkeit zur Verfügbarmachung dieses Mittel durch die Anzahl an darauf zurückgreifenden Tätigkeiten geteilt werden. Der Grund dafür ist, dass diese Tätigkeiten zur Verfügbarmachung im Konfigurationsprozess früher vorgeschlagen werden. Auch wenn sich der reale Aufwand der Tätigkeit nicht verändert, verringert sich damit der Gesamtaufwand zur allgemeinen Bedürfnisbefriedigung.

3.  *Bei Mitteln, die sich beim Teilen vermehren:* Eine einzige Ausführung der Tätigkeit deckt den Bedarf sämtlicher Konfigurationen, die dieses Mittel benötigen. Auf die jeweilige Konfiguration bezogen, wird daher der dafür notwendige Aufwand immer geringer, wodurch zur Feststellung des Gesamtaufwandes einer Konfiguration der Aufwand einer solchen Tätigkeit schlicht durch die Menge der darauf verweisenden Tätigkeiten dividiert werden könnte. Das Problem allerdings: Tätigkeiten, die Resultate hervorbringen, die sich beim Teilen vermehren, sind niemals Tätigkeitsmuster. Tätigkeitsmuster sind immer die Beschreibungen von Tätigkeiten, die sich im gesellschaftlichen Re-Produktionsprozess wiederholen – das heißt, die nicht einzigartig sind. Solche Tätigkeiten allerdings, die Mittel als Resultate hervorbringen, die sich durch Teilen vermehren (der Inhalt von Büchern, Methoden zur Konfliktlösung, Software, usw.), müssen niemals wiederholt werden, da nach ihrer Ausführung das Resultat offen und unbegrenzt verfügbar ist. Es handelt sich dabei um einzigartige Tätigkeiten, bei denen daher auch kein durchschnittlicher Aufwand festgestellt werden kann. Im Kapitel →zugeschriebene Anerkennung wird näher darauf eingegangen.
