---
sidebar_position: 3
slug: /momente-des-gesamtaufwandes
---

Der *Gesamtaufwand einer Konfiguration* soll jeglichen Aufwand umfassen, welcher zur Befriedigung eines bestimmten Bedürfnisses notwendig ist. Rein in dieser analytischen Perspektive ist es dabei irrelevant, welche Aspekte des Aufwandes hier hinzugezählt werden. Im Laufe des Kapitels wird sich allerdings auf die Einheit ‚*Zeitdauer‘* begrenzt, da diese als Einheit des Konfigurationsprozesses für diesen alleine von Bedeutung ist und dieser →[*Konfigurationsprozess*](/docs/konfigurationsprozess) auch die wesentliche Ursache ist, warum wir uns überhaupt mit dem Gesamtaufwand beschäftigen müssen.

Der durchschnittliche Gesamtaufwand einer Konfiguration lässt sich dabei leicht herausstellen, wenn wir die durchschnittliche Dauer jeder einzelnen Tätigkeit dieser Konfiguration kennen. Und genauso lässt sich der durchschnittliche *Gesamtaufwand einer Tätigkeit* herausstellen, welcher den Aufwand eben jener Tätigkeit plus sämtlichen Aufwand umfasst, welcher zur und durch die Ausführung der Tätigkeit notwendig wird. Dieser *Gesamtaufwand einer Tätigkeit* wird wieder im nachfolgenden Kapitel →[*der Konfigurationsprozess*](/docs/konfigurationsprozess) von tragender Bedeutung sein.

Folgend werden wir uns dem Gesamtaufwand einer Konfiguration über drei Momente annähern: 1. Über die *Verfügbarkeit von Mitteln und Tätigkeitsmustern*. 2. Über die *Verfügungsmöglichkeiten konkreter Personen über Mittel*. Und 3. über die Auswirkungen der Tätigkeit auf Mittel und die nicht-menschliche Natur, welche über *Nebenresultate des Tätigkeitsmusters* festgehalten werden und Tätigkeiten zur Erhaltung von Mitteln bzw. Lebensförderung nicht-menschlicher Natur nach sich ziehen können.

## (Lokale) Verfügbarkeit von Mitteln und Tätigkeitsmustern

Der Gesamtaufwand einer Konfiguration steht in unbedingten Zusammenhang mit den lokal verfügbaren Mitteln und der Anzahl der insgesamt vorhandenen Tätigkeitsmuster. Zwischen diesen verfügbaren Mitteln und den Tätigkeitsmustern gibt es immer eine Wechselwirkung: Gibt es keine entsprechenden Mittel, können die in den Tätigkeitsmustern beschrieben Tätigkeiten nicht ausgeführt werden. Gibt es keine entsprechenden Tätigkeitsmuster, bleibt es der Software verborgen, wie verfügbare Mittel angewendet werden können, um bestimmte Resultate und letztendlich bestimmte Formen der Bedürfnisbefriedigung zu erreichen.

Wieder am Beispiel des Bedürfnisses nach einer bestimmten Form des *künstlerischem Ausdrucks*, welches durch *Pinsel*, *Leinwand *und *Farbe* befriedigt werden kann. Im Kapitel →*Konfigurationen* sind wir davon ausgegangen, dass die *Leinwand* nicht zur Verfügung stand. Hätte sie zur Verfügung gestanden, dann wäre der *Aufwand* eingespart worden, welcher notwendig ist, sie verfügbar zu machen. Und diese Leinwand steht nicht zur Verfügung, dafür dürfen aber im lokalen Umfeld die Mittel *Spannvorrichtung, Hammer, Leinengewebe, Keilrahmen und Nägel* zum Zweck des Commonings verwendet werden – welche zufällig genau die Bedarfe eines Tätigkeitsmusters zur Herstellung einer Leinwand sind (*\#HstLw*). Gäbe es dieses Tätigkeitsmuster nicht, dann könnte die Software keinen Zusammenhang zwischen dem Bedürfnis nach *künstlerischer Auslebung* und eben jenen Mitteln herstellen. Erst durch das Tätigkeitsmuster entsteht eine Möglichkeit im lokalen Umfeld das Bedürfnis zu befriedigen, auch wenn eben menschliche Tätigkeit und damit also neuer *Aufwand* verbunden ist. - Ein neues Problem würde dagegen entstehen, wenn etwa das Mittel *Leinengewebe* nicht lokal verfügbar wäre. Gäbe es keine anderen lokal verfügbaren Mittel, durch welche dieses *Leinengewebe* verfügbar gemacht werden kann und gäbe es kein Tätigkeitsmuster, welche eben diese *Verwandlung* dieser Mittel in ein Leinengewebe beschreiben würde, dann könnte das Bedürfnis wieder nicht befriedigt werden.

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';


<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/3/light/Veränderung von Mitteln_linie.svg'),
    dark: useBaseUrl('/img/Textreihe/3/dark/Veränderung von Mitteln_linie_dark.svg'),
  }}
/>

## Verfügungsmöglichkeiten konkreter Personen über Mittel

Ein *Commons* als soziale Form verstanden, ist nicht an eine bestimmte Eigentumsform gebunden, sondern schlicht die Bezeichnung für jegliches Mittel das in einem *Commoning-* Prozess verwendet wird. Und womit wir im Rahmen des *ununterbrochenen Commonings* immer umgehen, sind also genau solche Commons, doch besonders auch im Bezug auf den Gesamtaufwand einer Konfiguration stellt sich die Frage, wer bestimmte Mittel für Commoning-Prozesse verwenden darf und wer von der Verwendung bestimmter Mittel ausgeschlossen ist.

Inwiefern wirken sich diese Verfügungsmöglichkeiten konkreter Personen über Mittel auf den Gesamtaufwand aus? Insofern, dass Konfigurationen entweder mehr oder weniger Aufwand benötigen, je nachdem, wer sich bestimmten Tätigkeiten annimmt und über welche Mittel diese Personen verfügen können. Nehmen wir das Tätigkeitsmuster '\#HstKr zur Herstellung eines Keilrahmens durch (a) Winkelsäge, (b) Holzleisten, (c) Nägel und (d) Werkbank'. An dieser Stelle werden die Beteiligten, welche sich der Tätigkeit potentiell zuordnen können oder wollen, in Gruppen geteilt, je nachdem über welchen *Bedarf der Tätigkeit* sie verfügen bzw. eben nicht verfügen. Und falls sich jemand zuordnen würde, der oder die über eine Winkelsäge verfügt *und sie auch zur Ausführung der Tätigkeit verwenden will*, dann entfällt selbstverständlich der Aufwand von Tätigkeiten (wie der *Ortsveränderung einer Winkelsäge), *über welche diese Winkelsäge verfügbar gemacht werden würde.

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/3/light/Konfiguration_Personen.svg'),
    dark: useBaseUrl('/img/Textreihe/3/dark/Konfiguration_Personen_dark.svg'),
  }}
/>


Je mehr Beteiligte also über ein Mittel verfügen können, desto höher ist die Wahrscheinlichkeit, dass zusätzlicher Aufwand zur Bedarfsdeckung einer Tätigkeit entfällt und desto geringer ist tendenziell der gesamte Aufwand, der zur Befriedigung eines bestimmten Bedürfnisses notwendig ist. Je mehr Beteiligte allerdings über dasselbe Mittel verfügen können, desto geringer ist tendenziell die Wahrscheinlichkeit, dass sich zuverlässige interpersonale Strukturen etablieren können, in denen spontan auf diese Mittel zurückgegriffen werden kann (siehe auch: →[*integrierte Zusammenschlüsse*]). Eine Struktur zur Verfügung über Gemeingüter so aufzubauen, dass sie effizient zur allgemeinen Bedürfnisbefriedigung ist und diese sich für die Betroffenen auch fair anfühlt, ist daher alles andere als einfach. Dem Problem wird sich im Kapitel →[*Nutzungsbedingungen von Mitteln*] näher angenommen, ist aber grundsätzlich ein Gegenstand des →[*sozialen Prozesses*].


## Nebenresultate und Erhaltungszustände

Bisher haben wir nur mit Tätigkeiten zu tun gehabt, welche sich direkt auf vermittelte Bedürfnisse beziehen und dabei Tätigkeiten außer acht gelassen, die notwendig sind, um eben solche Tätigkeiten zu gewährleisten. Die außer acht gelassenen Tätigkeiten sind solche zur *(Wieder-)Herstellung von Erhaltungszuständen* von Mitteln, welche ebenfalls in den *Gesamtaufwand einer Konfiguration* hineinfallen. Unter ‚Mittel‘ fällt an dieser Stelle auch die nicht-menschliche Natur (Tiere, Wälder, Flüsse, etc.), insofern sie im gesellschaftlichen Re-Produktionsprozess als *Mittel zum Zweck* verwendet wird bzw. davon betroffen ist. Als Teil des Lebens, welches durch Commoning gefördert werden kann und soll, kann damit allerdings anders umgegangen als mit etwa Maschinen oder anderem Werkzeug. Darauf wird im Kapitel →[*Bedürfnisvermittlung (empathisch)*] weiter eingegangen, während sich nachfolgend auf die *Mittel*-Bezeichnung beschränkt wird.

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/3/light/Mittelarten-resultat.svg'),
    dark: useBaseUrl('/img/Textreihe/3/dark/Mittelarten-resultat_dark.svg'),
  }}
/>

&nbsp;


Tätigkeiten machen nicht nur Resultate verfügbar, die im Prozess einer Bedürfnisbefriedigung benötigt werden, sondern wirken sich auch auf Mittel und die nicht-menschliche Natur aus. Während der Ausführung der Tätigkeiten können Mittel verbraucht, abgenutzt, verschmutzt, etc. werden oder es können auch neue Mittel entstehen; z.B. können sich Mittel ‚aufspalten‘ und ein Teil geht in das Resultat der Tätigkeit ein und das andere – ein Trägermittel etwa – bleibt übrig. Alles was durch die Tätigkeit geschieht, aber nicht auf die Bedürfnisbefriedigung abzielt, deren Zweck die Tätigkeit hat, wird als *Nebenresultat* einer Tätigkeit bezeichnet. Diese Nebenresultate können 1. *neue Mittel* und 2. *Zustandsveränderungen von Mitteln* sein.

Die Zustandsveränderung eines Mittels bzw. das neue Mittel selbst erzeugt allerdings noch keinen neuen Aufwand. Neuer Aufwand entsteht erst, wenn eine *Konsequenz* aus der Auswirkung einer Tätigkeit entsteht – wenn also eine verbrauchte Patrone aufgefüllt, ein verschmutzter Raum wieder geputzt [Anmerkung], eine verschlissene Maschine gewartet werden muss oder das Trägermittel zurück in eine Lagerhalle gebracht werden soll. Diese Mittel haben also einen *Erhaltungszustand* und dieser muss *(wieder-)hergestellt* werden.

> Wie im zweiten Teil der Textreihe angemerkt, wird von dem „Raum“ als Bedarf der Einfachheit halber meist abstrahiert, wodurch allerdings in den Konfigurationen notwendige Tätigkeiten wie „Putzen“ unsichtbar
bleiben. In der Software selbst werden derlei Tätigkeiten wie selbstverständlich sichtbar, wenn auch im weiteren Verlauf der Textreihe weiter von deren Darstellung abgesehen wird.

Die Vorstellungen und Grenzen der Erhaltungszustände von Mitteln und nicht-menschlicher Natur können dabei voneinander abweichen und müssen in einem →[*sozialen Prozess*] geklärt werden; wenn es etwa um die *Sauberkeit* eines konkreten Raumes oder die *artgerechte Haltung* eines konkreten Tieres geht. Die Definition des Erhaltungszustandes muss daher immer an den *konkreten Mitteln* bzw. den entsprechenden Teilen nicht-menschlicher Natur vorgenommen werden [Anmerkung]. Die Veränderung der Erhaltungszustände wird dabei, insofern es ohne menschliche Dokumentation möglich ist, durch die *Nebenresultate* von Tätigkeitsmustern festgehalten. Das bedeutet: Im Tätigkeitsmuster selbst muss festgehalten werden, wie sich die Tätigkeit *im Durchschnitt* auf die verwendeten Mittel auswirkt.

>  In den Mittelmustern können selbstverständlich Vorlagen gespeichert sein, mit denen sich die Erhaltungszustände der konkreten Mittel, welche diesen Mittelmustern untergeordnet sind, leichter definieren lassen.

Der Erhaltungszustand selbst macht aber immer noch keinen Aufwand aus: Es müssen daher *Tätigkeiten definiert werden*, welche die Erhaltungszustände der entsprechenden Mittel bzw. der nicht-menschlichen Natur (wieder-)herstellen. Und dabei muss nicht nur definiert werden, *welche* Tätigkeiten ausgeführt werden müssen, sondern auch *wann* die Ausführung geschehen soll. Ein Raum muss nicht nach jeder darin stattfindenden Tätigkeit geputzt, eine Maschine nicht nach jeder Tätigkeit gewartet werden etc. pp. Diese Frage nach dem ‚wann‘ wird schließlich im Abschnitt *Vorschlag von Tätigkeiten: Wiederherstellung von Erhaltungszuständen* des Kapitals →[*der Konfigurationsprozess*](/docs/konfigurationsprozess) näher betrachtet. Die Tätigkeiten zur (Wieder-)Herstellung von Erhaltungszuständen haben dabei selbstverständlich wieder eigenen Bedarf und können sich wiederum auf Mittel und nicht-menschliche Natur auswirken etc. pp.

An dieser Stelle sind also zusammengefasst folgende Punkte notwendig, damit in der Konfiguration und damit auch im *Gesamtaufwand* die Auswirkungen der Tätigkeiten mit einbezogen werden können:

1.  Es müssen *Erhaltungszustände *der konkreten Mittel definiert werden.

2.  Es muss definiert werden, wie sich Tätigkeiten *auf verwendete und betroffene Mittel auswirken* und welche *neuen Mittel dabei entstehen* (Nebenresultate)

3.  Es muss definiert werden, welche Tätigkeiten geeignet sind und wann diese ausgeführt werden sollen, um bestimmte Mittel wieder *in ihren Erhaltungszustand zurückzuführen.*

Ein Beispiel um das Prinzip von Erhaltungszuständen und Nebenresultaten zu verdeutlichen: In der nebenstehenden Grafik geht es wieder um die Herstellung der Leinwand (T1b1), deren Bedarf nach Leinengewebe wieder einmal nicht verfügbar ist, aber über eine einfache Ortsveränderung verfügbar gemacht werden kann (T1b1c1). Auf das vermittelte Bedürfnis bezogen wären diese beiden Tätigkeiten ausreichend. Ein Nebenresultat der Leinwand-Herstellung ist allerdings die Zustandsveränderung des Raumes, welcher im Prozess schmutzig oder unordentlich werden kann. Der *Erhaltungszustand* des Raumes muss also *wiederherstellt* werden, sprich, er muss gereinigt werden (T1b101). Je nachdem, wie häufig bzw. wie intensiv der Raum gereinigt werden muss und wie viele andere Tätigkeiten in diesem Raum stattfinden, wird die entsprechende Tätigkeit einen unterschiedlichen *durchschnittlichen *Aufwand nach sich ziehen, der zum Gesamtaufwand der Leinwand-Herstellung hinzugerechnet werden muss.

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/3/light/spekAufwand.svg'),
    dark: useBaseUrl('/img/Textreihe/3/dark/spekAufwand_dark.svg'),
  }}
/>


In der *Ortsveränderung* des Leinengewebes durch einen PKW sind zwei Nebenresultate hervorgehoben: Die Abnutzung des PKWs und der Ausstoß von CO2 durch das Verbrennen von Benzin. Ersteres bezieht sich direkt auf den Zustand eines verwendeten Mittels (des PKWs), bei welchem entsprechend geregelt werden muss, wie oft dieser überprüft/gewartet werden sollte. Auch hier werden auf das jeweilige Modell bezogene *Durchschnittswerte* benötigt, die sich mit der Zeit einpendeln können. Wenn sich ergibt, dass für ein bestimmtest Modell alle 10.000km Reparaturen ergeben, die im Durchschnitt und unabhängig von der konkreten Form der Reparatur 10 Stunden andauern, dann kann dieser *anteilige Aufwand* der Verwendung des PKWs gemäß der gefahrenen Strecke der Tätigkeit T1b1c1 zugeschlagen werden.

Der *Ausstoß von CO2* in die Atmosphäre betrifft prinzipiell *alle* Menschen und von der Regelung des Erhaltungszustandes der Atmosphäre darf daher niemand strukturell ausgeschlossen sein; was schließlich einen →[*sozialen Prozess*] einer besonderen Art notwendig macht. Wenn aber ein solcher Erhaltungszustand definiert wurde, dann braucht es immer eine Tätigkeit, welche den CO2-Pegel wieder senkt, falls er durch eine Tätigkeit steigt. Und der Aufwand dieser Tätigkeiten zur Reduzierung des CO2-Wertes muss also zum Aufwand der Tätigkeit der Ortsveränderung ebenso hinzugerechnet werden. Erst so zeigt sich, welchen *Gesamtaufwand* der Prozess zur Befriedigung eines Bedürfnisses die Ortsveränderung des Leinengewebes (durch einen PKW) wirklich nach sich zieht.

Im Bezug auf die *Nebenresultate* lässt sich dabei herausstellen, welche Tätigkeiten einen vergleichsweise geringen Gesamtaufwand mit sich bringen:

1.  Tätigkeiten, die auf langlebige Mittel zurückgreifen.

2.  Tätigkeiten, die auf Mittel zurückgreifen, welche leicht in ihren Erhaltungszustand zurückgeführt werden können.

3.  Tätigkeiten, bei deren Ausführung tendenziell wenig und bevorzugt leicht recycelbaren Müll produziert wird.

4.  Tätigkeiten, deren Ausführung energiesparend ist.

Durch den Einbezug der Nebenresultate einer Tätigkeit und den entsprechenden Erhaltungszustand von Mitteln, ist eine *unaufwändige Konfiguration* auch tendenziell eine *nachhaltige Konfiguration.* Da im →[*Konfigurationsprozess*](/docs/konfigurationsprozess) der Aufwand von Tätigkeiten ausschlaggebend ist, ob diese vorgeschlagen werden, kann hierdurch eine gesellschaftliche *Bewegungstendenz* zur vermehrten Ausführung von Tätigkeiten mit genau diesen Eigenschaften entstehen.
