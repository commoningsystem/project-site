---
sidebar_position: 1
slug: /gcs-erstes-vorwort
---

*Marcus Meindel & das Global Commoning System-Projektteam*

Willkommen auf einer siebenteiligen Reise einmal quer durch die Struktur einer Software, welche, zumindest der Theorie nach, die Aufhebung der kapitalistischen Produktionsweise in eine emanzipatorische Richtung möglich machen soll. Das ist durchaus ernst gemeint, muss aber an dieser Stelle nicht ernst genommen werden. Und selbstverständlich ist diese Software auch nicht alles, was zur Überwindung der heutigen gesellschaftlichen Problematiken notwendig ist. Allerdings entsteht durch sie eine Möglichkeit, die Welt zu erfassen, um darin selbstorganisierte Kooperationsstrukturen aufzubauen, deren Zweck nichts anderes als die Befriedigung menschlicher Bedürfnisse ist und welche sich lediglich durch die Fähigkeiten und Interessen der Beteiligten strukturieren.

Es ist ernst gemeint: Durch das Internet ist eine Gesellschaftsform denkbar geworden, in der wir uns nicht als Konkurrenten gegenübertreten müssen, in der wir auf Bürokratieapparate und Planungskomitees verzichten können, in denen wir uns unseren Bedürfnissen, Fähigkeiten und Interessen nach entwickeln können und kurz: in der der Mensch dem Menschen ein Helfer wird, ohne, dass wir die technischen Errungenschaften hinter uns lassen müssen. Aber diese *Vermittlungsform*, die das alles möglich macht, entsteht nicht von selbst – sie muss mit den gegebenen Möglichkeiten konstruiert und verbreitet werden. Und das ist die Aufgabe, welche jetzt vor uns liegt.

Wenn ich auch denke, dass über diese Textreihe die Struktur des *Commonings* im allgemeinen verständlicher werden kann, richtet sie sich sich in erster Linie an Entwickler und Entwicklerinnen. Und sowie jeder Text, der mit der Software in Zusammenhang steht, selbstverständlich unter einer *Creative-Commons-*Lizenz läuft, ist die Entwicklung als *Freie Software* eine unbedingte Voraussetzung.

In diesem ersten Teil werden zuerst theoretische Grundlagen, die Strukturformel des Commonings und schließlich die Grundstruktur der Software mit den wesentlichen Handlungsmöglichkeiten für die Anwender und Anwenderinnen eingeführt. Am Ende dieses Teils steht eine kurze Übersicht zu den folgenden sechs Teilen. Parallel zur Textreihe ist der Essay [„The Timeless Way of Re-Production“ (2019)](/docs/timeless-way-of-re-production) erschienen, in welchem auf die Softwarestruktur durch eine Interpretation der Mustertheorie von Christopher Alexander geschlossen wurde.

In diesem Sinne: Ich freue mich sehr, dass du zu diesem Text gefunden hast und wäre von Herzen dankbar, wenn du dich – auf welche Weise auch immer und in welchem konkreten Projekt auch immer – an der Realisierung dieser Vermittlungsform beteiligst.
