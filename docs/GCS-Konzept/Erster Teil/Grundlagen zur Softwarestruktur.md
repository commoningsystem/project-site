---
sidebar_position: 4
slug: /grundlagen-softwarestruktur
---

Die Struktur der Software wird im Verlauf der Textreihe im Detail dargestellt. Folgend geht es daher nur um die *Grundstruktur *und wie sich eine einzelne Person darin einbringen kann.

Da der Zweck der Software die Unterstützung von Prozessen der Bedürfnisbefriedigung ist, müssen diese Bedürfnisse (B-) natürlich vermittelt werden können. Jedes Bedürfnis wird über eine Tätigkeit befriedigt – hier in der Grafik wird diese Tätigkeit „T1“ genannt. Zur Ausführung der Tätigkeit T1 braucht es *das* *Mittel (a)* bzw., auf die Tätigkeit bezogen, *M1a*. Das Mittel *M1a *kann über die Tätigkeit *T1a1* verfügbar gemacht werden.

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';


<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/1/light/Grundstruktur.svg'),
    dark: useBaseUrl('/img/Textreihe/1/dark/Grundstruktur_dark.svg'),
  }}
/>;

Wer führt diese Tätigkeiten aus? Da wir uns in einer Struktur bewegen, in welcher Personen niemals über andere Personen bestimmen dürfen, kann die Zuordnung zu notwendigen Tätigkeiten nur durch die jeweiligen Personen selbst geschehen. Wir nennen das den Prozess der *Selbstzuordnung* [Anmerkung], welcher über verschiedene Softwarefunktionen unterstützt werden soll. Die *Kooperation* selbst geschieht zwischen den Personen, welche die aufeinander bezogenen Tätigkeiten ausführen. Und an dieser Stelle angemerkt: *Immer wenn von einer Person gesprochen wird, ist auch immer eine Gruppe damit gemeint*. Ob eine Person alleine oder eine Gruppe gemeinsam sich in die Softwarestruktur einbringt, ist nicht relevant.

> *Das Konzept der *Selbstauswahl* im Rahmen des Commonings wurde besonders von Meretz/Sutterlütti im Rahmen der *„commonistischen Stigmergie“ eingebracht (z.B. Kapitalismus aufheben, S.178)

Bei den Mitteln, welche bei den jeweiligen Tätigkeiten verwendet werden, unterscheiden wir zwischen zwei Kategorien, wobei die Grenzen dazwischen fließend sind: *Private Mittel *und *Gemeingut*. Private Mittel sind Eigentum einer konkreten Person, welche über deren Nutzung alleine bestimmen darf. Sie kann sich entscheiden, diese Mittel nur selbst zu verwenden oder sie kann *Nutzungsbedingungen* festlegen, in denen auch andere diese Mittel mitverwenden dürfen. Je nachdem, welche Person sich daher einer Tätigkeit zuordnet, kann sich demnach auch unterscheiden, welche Mittel noch für diese Tätigkeit verfügbar gemacht werden müssen.

Für die soziale Form des *Commons* sind solche Eigentumsverhältnisse irrelevant, wenn auch bei privaten Eigentum die ständige *Ausgrenzung *durch die Eigentümerin droht. Anders ist das bei Gemeingütern, auch wenn es sich hierbei um keine klare Kategorie handelt, die aber im vierten Teil der Textreihe näher aufgeschlüsselt werden soll. An dieser Stelle gehen wir verkürzt davon aus, dass jedes Mittel, das über eine Tätigkeit im Rahmen des Commonings verfügbar gemacht wird, ein Gemeingut ist und niemand von dessen Verwendung ausgeschlossen wird.

Über die Verwendung von Commons können *Absprachen *und *Regeln* getroffen werden, sowie *Sanktionen* bei Regelverletzung und etwa *Nutzungseinschränkungen* um zum Beispiel die Übernutzung von Naturvermögen zu verhindern. Dass niemand von der Verwendung Gemeingütern strukturell ausgeschlossen ist, bedeutet für die Software, dass um jedes gesellschaftliche Mittel ein *sozialer Prozess* entstehen können muss, in welchem die Verwendung geklärt werden kann. Dieser soziale Prozess muss durch entsprechende Kommunikationsfunktionen oder etwa die Transparenz von Absprachen unterstützt werden. Neben der *Bedürfnisvermittlung*, der *Selbstzuordnung*, dem *zur-Verfügung-stellen* von privaten Mitteln ist das Recht auf die *Mitentscheidung zur Verwendung der zur Verfügung stehenden Mittel* die letzte grundlegende Handlungsmöglichkeit der Anwender und Anwenderinnen.
