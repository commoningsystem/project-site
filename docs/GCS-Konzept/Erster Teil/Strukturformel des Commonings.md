---
sidebar_position: 3
slug: /strukturformel
---


Im Grunde genommen ist Commoning alles, was selbstverständlich ist, wenn ich mich um meine Mitmenschen sorge und wir uns untereinander mit dem Ziel organisieren, dass es uns allen gut geht. Damit daraus aber auch komplexe Strukturen entstehen können, müssen wir uns der Sache strukturell annähern.

Folgend wird der Prozess dargestellt, vom Bedürfnis über seine Vermittlung und den kooperativen Prozess zu seiner Befriedigung. Ein Bedarf entsteht dabei immer nur, wenn ein *Mittel* für eine zur Bedürfnisbefriedigung notwendigen Tätigkeit benötigt wird. Diese Mittel selbst unterscheiden wir danach, wie sie *geteilt* werden können. Das heißt, ob sie *aufgeteilt* werden müssen, da sie sich in ihrer Verwendung abnutzen oder aufbrauchen. Ob sie *gemeinsam genutzt* werden können, was entsprechende Regelungen zur gemeinsamen Nutzung nach sich ziehen kann. Oder ob sie *verbreitet* werden können, da sie sich – wie Informationen, Ideen, Codes, etc. - durch den Prozess des Teilens vermehren.

> Hier folgen wir Helfrich/Bollier. Vgl. *Frei, Fair und Lebendig, *S.85, wobei das von ihnen verwendete „weitergeben“ mit dem m.M.n. für die Sache klareren „verbreiten“ ersetzt wurde.

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Formeln/Commoning_light.svg'),
    dark: useBaseUrl('/img/Formeln/Commoning_dark.svg'),
  }}
/>


**Einsicht:** An dieser Stelle gibt es drei Möglichkeiten, Tätigkeiten im gesellschaftlichen Reproduktionsprozess anzustoßen: Jemand wird sich einem eigenen Bedürfnis oder dem Bedürfnis einer anderen Person bewusst (*B-*). Jemand erkennt den Bedarf nach einem Mittel (*M-*), der für eine Tätigkeit im Rahmen des Commonings notwendig ist. Oder jemand erkennt den problematischen Zustand eines Mittels (*Z-*) [Anmerkung]. Diese *Einsichten*, Bedürfnisse ausgeklammert, können unter bestimmten Bedingungen auch ohne menschliches Einwirken vermittelt werden.


> Die dritte Möglichkeit wird erst ab dem dritten Teil der Textreihe relevant (→[*Momente des Gesamtaufwandes*](/docs/momente-des-gesamtaufwandes))

**Vermittlung:** Alle drei Möglichkeiten können außerhalb oder innerhalb der Softwarestruktur vermittelt werden. Außerhalb der Software ist die Vermittlung dabei abhängig von persönlichen Kontakten und anderen Strukturen, die der jeweiligen Person bekannt sind. Für uns besonders relevant ist die Vermittlung innerhalb der Softwarestruktur, in welcher Bedürfnisse, Bedarfe und problematische Zustände von Mitteln – in welcher Form auch immer – eingespeist und ausgelesen werden können.

**Commoning:** Die Software unterstützt den Prozess des Commonings. Relevant für die Softwarestruktur ist dabei, dass es immer um konkrete menschliche *Tätigkeiten *geht, deren *Kooperation auf Augenhöhe *verläuft und bei denen (in der Regel) *Mittel verwendet *werden.

**Abschluss:** Eine Tätigkeit ist abgeschlossen, wenn damit entweder ein Bedürfnis befriedigt ist (*B+*), ein Bedarf nach einem bestimmten Mittel gedeckt (*M+*) oder der Erhaltungszustand eines Mittels (wieder-)hergestellt wurde (*Z+*).
