---
sidebar_position: 5
slug: /bibliothek
---

Die ‚Bibliothek‘ ist ein Werkzeug, um Beteiligte darin zu unterstützen, sich Tätigkeiten anzunehmen, die in ihrem eigenen Interesse sind. Die Bibliothek ist dabei eine persönliche Sammlung von Tätigkeitsmustern.

Ein Tätigkeitsmuster kann dabei *manuell* gesucht und schließlich in der eigenen Bibliothek gespeichert werden oder das Tätigkeitsmuster wird *automatisch* in die Bibliothek übertragen, nachdem die Person sich bereits mindestens einmal einer entsprechenden Tätigkeit zugeordnet hat.

Die Tätigkeitsmuster können in der Bibliothek entsprechend markiert werden, ob sich der Tätigkeit *gerne* oder *nicht gerne* angenommen wird. Je nachdem kann die Person durch die Software *benachrichtigt* werden, sobald eine Tätigkeit im lokalen Umfeld *vorgeschlagen *wird* (→ [Konfigurationsprozess](/docs/konfigurationsprozess)), *an welcher Interesse besteht oder die Person wird, wenn überhaupt, erst benachrichtigt, wenn die Tätigkeit eine hohe →[*Wichtigkeit*] hat.

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';


<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/2/light/bib.svg'),
    dark: useBaseUrl('/img/Textreihe/2/dark/bib_dark.svg'),
  }}
/>
