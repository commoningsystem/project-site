---
sidebar_position: 6
slug: /faehigkeiten
---

*Kurze Anmerkung: Fähigkeiten sind zwar als Funktion der Software notwendig, aber nicht unbedingt zum Verständnis der grundlegenden Struktur des Softwarekonzeptes. Wird die Textreihe chronologisch gelesen, kann dieses Kapitel übersprungen werden.*

In der ‚Bibliothek‘ gibt es die Möglichkeit, einzelne Tätigkeitsmuster als eigene *Fähigkeiten* zu definieren. Im Gegensatz zu den im nächsten Kapitel beschriebenen Qualifikationen, sind Fähigkeiten dabei auf Softwareebene keine Bedingungen, um bestimmte Tätigkeiten ausführen zu können. Die Definition von Fähigkeiten hilft Beteiligten vielmehr dabei, den Gesamtprozess übersichtlicher zu gestalten und die Selbstzuordnung zu vereinfachen. Fähigkeiten werden dabei als Tätigkeiten verstanden, deren Ablauf *verinnerlicht* ist. Es sind also Handlungen, die Beteiligte prinzipiell ohne die Beschreibung im jeweiligen Tätigkeitsmuster durchführen können bzw. nach eigener Einschätzung wissen, was bei den entsprechenden Problemen getan werden muss. Die entsprechenden Tätigkeitsmuster können daher durch Beteiligte ihrem eigenen Ermessen nach in der Bibliothek als Fähigkeiten markiert werden.

Durch diese Angabe, dass bestimmte Tätigkeiten *verinnerlicht* sind, können sich Beteiligte schließlich *komplexeren Tätigkeitsmustern* annehmen. Um das verständlich zu machen, wird weiter auf die Produktion von Leinengewebe zurückgegriffen, welche auch Marx in seiner Wertformanalyse verwendet hat. Die nachfolgende Darstellung der Herstellung von Leinengewebe ist dabei stark vereinfacht und im Sinne der Übersichtlichkeit werden die Tätigkeitsmuster von ihrem Inhalt unabhängig als #A-F bezeichnet.

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';


<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/2/light/Fähigkeiten(bib).svg'),
    dark: useBaseUrl('/img/Textreihe/2/dark/Fähigkeiten(bib)_dark.svg'),
  }}
/>;

Damit das ‚Leinengarn‘ (\#F - a) im Webstuhl verarbeitet werden kann, müssen die Fäden des Garns erst die gleiche Länge und die richtige Reihenfolge erhalten. Dafür wird es auf den ‚Scherbaum‘ (\#F - b) aufgespannt. Das so bearbeitete Leinengarn wird schließlich als ‚Kette‘ bezeichnet. Diese ‚Kette‘ (\#E - b) wird schließlich auf den bisher ‚unbespannten Webstuhl‘ (\#E - a) gespannt. Im Webprozess werden aus dieser Kette die vertikal verlaufenden Fäden des Gewebes entstehen. Ein weiteres ‚Leinengarn‘ (\#D - b) wird schließlich gemäß einer ‚Leinwand-Bindungspatrone‘ (\#D - c) durch die Litzen und Blätter des ‚mit Kette bespannten Webstuhls‘ (\#D - a)* *gestochen. Die Leinwand-Bindungspatrone gibt dabei vor, wie die Fäden durch den Webstuhl verlaufen müssen, damit am Ende das gewünschte Gewebemuster herauskommt. Durch den damit vollständig ‚bespannten Webstuhl‘ (\#C - a) kann das ‚Leinengewebe‘ hergestellt



In der Grafik ist die Bibliothek einer Person zu sehen, welche sich sämtliche gerade aufgelisteten Tätigkeiten in ihrer Bibliothek gespeichert hat und diese Person könnte bei jeder einzelnen Tätigkeit durch die Software entsprechend benachrichtigt werden, sobald diese *vorgeschlagen* wird. Nur drei der vier Tätigkeitsmuster, auf welche die vorgeschlagenen Tätigkeiten verweisen, sind dabei auch als *verinnerlicht*, also als eigene ‚Fähigkeiten‘ (F) markiert. Weiter sind in der Grafik zwei ‚komplexere Tätigkeitsmuster‘ zu sehen, welche jeweils zwei der einfachen Tätigkeitsmuster als Fähigkeiten voraussetzen. Statt sich jetzt den beiden, durch die Tätigkeitsmuster \#E und \#F beschriebenen Tätigkeiten einzeln zuzuordnen, könnte sich die entsprechende Person auch einer Tätigkeit zuordnen, die durch das komplexe Tätigkeitsmuster \#B beschrieben wird. Dagegen könnte sie sich *nicht* einer Tätigkeit zuordnen, die durch das komplexe Tätigkeitsmuster \#A beschrieben wird, da sie eine der beiden als Fähigkeiten vorausgesetzten Tätigkeitsmuster – das Tätigkeitsmuster \#C* –* nicht in ihrer Bibliothek als verinnerlicht markiert hat.

Ein komplexeres Tätigkeitsmuster besteht dabei *ausschließlich* aus der Kombination einfacherer Tätigkeitsmuster. Diese einfachereren Tätigkeitsmuster selbst – die Bezeichungen sind relativ zueinander – könnten dabei aber selbst wiederum komplexere Tätigkeitsmuster sein, mit denen einfache Tätigkeitsmuster zusammengefasst werden. Wesentlich ist, dass bei komplexeren Tätigkeitsmustern keine neuen Informationen hinzukommen und auf „unterster Ebene“ jeder Schritt genau beschrieben ist. Falls eine neue Information doch notwendig sein sollte, dann muss diese zuerst in Form eines *Tätigkeitsmusters* angelegt und schließlich ebenfalls als *Fähigkeit* des komplexeren Tätigkeitsmusters definiert werden.

Da das im Kapitel →[*Konfigurationen*](/docs/konfigurationen) vorgestellte Tätigkeitsmuster ‚Herstellung von Leinengewebe durch (a) unbespannten Webstuhl, (b) Scherbaum, (c) Leinengarn und (d) Leinwand-Bindungspatrone‘ (\#HstLg) *denselben* Bedarf hat wie die beiden zusammenhängenden komplexeren Tätigkeitsmuster *\#A* und *\#B* zusammen, kann \#HstLg selbst als komplexeres Tätigkeitsmuster gefasst werden, welches die beiden anderen komplexeren Tätigkeitsmuster als Fähigkeiten voraussetzt. Über *Fähigkeiten* werden also Tätigkeiten auf Softwareebene in kleinere Schritte geteilt bzw. werden einzelne Tätigkeiten zusammengefasst. Da prinzipiell jedes Tätigkeitsmuster als Fähigkeit markiert werden kann, kann sich prinzipiell auch jedem komplexen Tätigkeitsmuster zugeordnet werden. Vorausgesetzt ist hier jeweils, dass die definierten *Qualifikationen* der darin enthaltenen Tätigkeitsmuster gegeben sind.

Die Definition von Fähigkeiten hilft dabei den Beteiligten den Gesamtprozess einerseits schneller erfassen und sich leichter einbringen zu können, anderseits den organisatorischen Aufwand zu minimieren, welcher mit der Selbstzuordnung zu vielen kleineren Tätigkeiten unbedingt zusammenhängt.

Hierzu eine kurze Anmerkung zu den →[*Details von Tätigkeitsmustern*]: Dasselbe Tätigkeitsmuster hat im besten Fall eine Vielzahl von unterschiedlichen Beschreibungen für denselben Prozess. Die Sprache ist dabei natürlich ein wesentlicher Punkt, aber genauso die Form der Darstellung (als Textbeschreibung, Video, etc.) oder etwa der Detailreichtum der Beschreibung. Gibt es etwa eine zusätzliche sehr knappe Beschreibung eines Musters und ist diese entsprechend definiert, kann die Beschreibung eines komplexen Tätigkeitsmusters *automatisch generiert* werden, indem diese knappen Beschreibung aneinander gehängt werden.

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/2/light/Fähigkeiten(Skalierung).svg'),
    dark: useBaseUrl('/img/Textreihe/2/dark/Fähigkeiten(Skalierung)_dark.svg'),
  }}
/>;


Da ein komplexes Tätigkeitsmuster lediglich eine Verschachtelung von einfacheren Tätigkeitsmustern ist, ist sowohl der Bedarf als auch Aufwand (→[*Der Aufwand und die Einheit des Konfigurationsprozesses*](/docs/aufwand-und-einheitsbestimmung)) genau gleich. Der Aufwand eines komplexen Tätigkeitsmusters ist daher auch die Summe des Aufwandes der darin enthaltenen einfachen Tätigkeitsmuster. Und da ein Tätigkeitsmuster immer die eindeutige Beschreibung eines Prozesses ist, sind weiter auch die Tätigkeitsmuster, welche als dessen Fähigkeiten definiert sind, immer eindeutig. Einzelne Tätigkeitsmuster können dabei als Fähigkeiten in unterschiedlichen anderen (komplexeren) Tätigkeitsmustern definiert sein.
