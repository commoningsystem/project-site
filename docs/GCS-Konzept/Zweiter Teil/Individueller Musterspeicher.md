---
sidebar_position: 4
slug: /individueller-musterspeicher
---

Der ‚individuelle Musterspeicher‘ ist selbst keine Funktion, sondern weist auf zwei verschiedene Ebenen der Softwarevermittlung hin: Einmal den *(Tätigkeits-)Musterspeicher* einer konkreten Person innerhalb der Software, was nachfolgend als *‚Bibliothek‘* bezeichnet wird. Und einmal auf das *persönliche Wissen* einer konkreten Person außerhalb der Software, das heißt die *Erfahrungen* zur Ausführung von Tätigkeiten, die sie im Laufe ihres Lebens gemacht hat und welche sie einerseits *allgemein verfügbar* machen und anderseits als *Fähigkeit* innerhalb der Software angeben kann.
