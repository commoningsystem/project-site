---
sidebar_position: 3
slug: /konfigurationen
---

Eine *Konfiguration* ist im Allgemeinen die *Anordnung und Auswahl *von Tätigkeitsmustern zum Zweck einer bestimmten Bedürfnisbefriedigung und diese Konfiguration ist damit eine Beschreibung der besondere Form einer *Kooperation*, wie also Beteiligte gemeinsam Bedürfnisse befriedigen. Im Verlauf der Textreihe wird auch noch eingeführt, wie nicht-Software-vermittelte Zusammenschlüsse ebenfalls Teil solcher Konfigurationen werden können (→[*Integrierte Zusammenschlüsse*]).

Grundlegend ist, dass die Strukturelemente einer Konfiguration – also im Allgemeinen die Tätigkeitsmuster – nur miteinander verbunden werden können, wenn *der Bedarf einer Tätigkeit identisch ist mit dem Resultat der folgenden Tätigkeit*. Zwischen zwei Tätigkeitsmustern steht also immer das Muster eines Mittels.

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/2/light/Konfig-einfach.svg'),
    dark: useBaseUrl('/img/Textreihe/2/dark/Konfig-einfach_dark.svg'),
  }}
/>


Um den Fokus auf die innere Logik eines Commoning-Prozesses zu richten, wird das nicht an einem gegenwärtigen Beispiel verdeutlicht, sondern auf die Produktion eines Mittels zurückgegriffen, welches Marx in seiner Wertformanalyse des *Kapitals* verwendet hat: Die *20 EllenLeinwand* [Anmerkung] Durch die Mengenangabe „20 Ellen“ wird klar, dass er damit ‚Leinengewebe‘ und nicht den Kunstbedarf ‚Leinwand‘ meinte, welcher als Stückzahl angegeben wäre. Folgend werden die Begriffe ihrer alltäglichen Verwendung angepasst: ‚Leinwand‘ ist ein Mittel, auf dem gemalt wird und das aus einem ‚Leinengewebe‘ besteht, das auf einen ‚Keilrahmen‘ gespannt ist. Auf Softwareebene sind sowohl ‚Leinwand‘ als auch ‚Leinengewebe‘ und ‚Keilrahmen‘ verschiedene Muster von Mitteln, mit denen konkret vorhandene Mittel allgemein beschrieben werden.

> MEW23, S.79

Eine Konfiguration *beginnt* immer mit einem Bedürfnis und dieses Bedürfnis wird immer durch eine Tätigkeit befriedigt. Das erste Tätigkeitsmuster einer Konfiguration ist daher immer ein Tätigkeitsmuster, das *eine bestimmte Bedürfnisbefriedigung* als Resultat hat. Im gewählten Beispiel ist das Bedürfnis eine bestimmte Form der künstlerischen Ausdrucks, das nach Angabe der Person, welche das Bedürfnis vermittelt hat (→[*Vermittlung von Bedürfnissen*]), durch die Verwendung (Tätigkeit *T1*) von ‚Pinseln‘* *(M1a), einer ‚Leinwand‘* *(M1b) und ‚Farbe‘ (M1c) befriedigt werden kann.

In diesem Beispiel wird die Person, welche diese erste Tätigkeit durchführt, wie selbstverständlich auch die Person sein, welche auch das Bedürfnis vermittelt hat. Selbst wenn diese Person das Bedürfnis vermittelt hätte, über ein Kunstwerk zu reflektieren, würde sie *die Tätigkeit des Reflektierens* selbst durchführen und das Kunstwerk wäre als Mittel ein Bedarf dieser Tätigkeit. Anders dagegen verhält es sich bei Tätigkeiten, die wir heute als „Dienstleistungen“ bezeichnen oder sich oft auch im Bereich der gegenseitigen Fürsorge finden lassen. Das Bedürfnis nach ‚körperlicher Hygiene‘ einer pflegebedürftigen Person könnte so etwa nicht durch diese Person selbst befriedigt werden. Falls es sich daher nicht um eine →[*emphatische Bedürfnisvermittlung*] handelt, und das bezieht sich wieder auf die konkrete Entwicklung der Software, steht die erste Tätigkeit einer Konfiguration nicht zur Selbstzuordnung frei, sondern wird automatisch durch die Person besetzt, welche das Bedürfnis auch vermittelt hat.

Der Bedarf zur Ausführung der im Tätigkeitsmuster beschriebenen Tätigkeit kann entweder *verfügbar* oder *nicht verfügbar* sein. Ob ein bestimmtes Mittel verfügbar ist, ist dabei sowohl abhängig von der *lokalen Umgebung* in welcher der Bedarf vermittelt wird und welche Mittel *dort* den entsprechenden Nutzungsbedingungen nach für die Tätigkeit verwendet werden dürfen und weiter abhängig von der konkreten Person, welche sich der Tätigkeit zuordnet und welche Mittel *ihr persönlich* zur Verfügung stehen. Erst nach einer Selbstzuordnung zu einer Tätigkeit kann also festgestellt werden, welcher Bedarf vorhanden ist und welcher Bedarf noch zur Verfügung gestellt und daher vermittelt werden muss.

Jeder Bedarf kann dabei grundsätzlich durch verschiedene Tätigkeiten gedeckt werden. Hierfür sehen wir uns das Tätigkeit zur ‚Herstellung einer Leinwand‘ näher an, welche auf das Tätigkeitsmuster **#HstLw* verweist. Selbstverständlich ist das auch nur eine von vielen Möglichkeiten, eine 'Leinwand‘ verfügbar zu machen. Im Tätigkeitsmuster angegeben sind dabei fünf Bedarfe: (a) ein Hammer, (b) Nägel, (c) Leinengewebe, (d) Keilrahmen und (e) Spannvorrichtung [Anmerkung] Wir nehmen an, dass die Mittel ‚Hammer‘, ‚Nägel‘ und ‚Spannvorrichtung‘ der Person, welche sich der Tätigkeit annimmt, zur Verfügung stehen und die Mittel ‚Leinengewebe‘ und ‚Keilrahmen‘* *nicht.

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/2/light/Konfig-entfaltet.svg'),
    dark: useBaseUrl('/img/Textreihe/2/dark/Konfig-entfaltet_dark.svg'),
  }}
/>



> Ein notwendiges Mittel für die meisten Tätigkeiten ist ein entsprechender Raum. Um diesen nicht ständig neu aufzuführen und somit den Lesefluss zu behindern, wird in der gesamten Textreihe davon abstrahiert. Eine Ausnahme ist das Kapitel →[*Momente des Gesamtaufwandes*](/docs/momente-des-gesamtaufwandes)

Den Bedarf nach den beiden Mitteln kann die Person selbstverständlich auch außerhalb der Software an andere Personen oder andere Strukturen vermitteln. Wichtig wäre es daher, dass es innerhalb der Software die Funktion gibt anzugeben, wann mit der Verfügbarkeit der Mittel zu rechnen ist, um eine Transparenz des Kooperationsprozesses sicherzustellen. Hier nehmen wir jedoch weiter an, der Bedarf nach ‚Leinengewebe‘ und ‚Keilrahmen‘ wird über die Software vermittelt.

Das ‚Leinengewebe‘ kann zum Beispiel über eine Ortsveränderung von bestehender Leinwand verfügbar gemacht werden (Tätigkeitsmuster #OLg, Tätigkeit T1b1c1). Das entsprechende Muster wäre „Ortsveränderung von Leinengewebe durch (a) Leinengewebe, (b) Fahrzeug und (c) Treibstoff“ [Anmerkung]. Eine andere Möglichkeit ‚Leinengewebe‘ verfügbar zu machen, wäre selbstverständlich die Herstellung davon. Das in der Grafik verwendete Tätigkeitsmuster \#HstLg (T1b1c2) heißt entsprechend: „Herstellung von Leinengewebe durch (a) unbespannten Webstuhl, (b) Scherbaum, (c) Leinengarn und (d) Leinwand-Bindungspatrone“.

> Die „Ortsveränderung“ ist dabei ein besonderes Muster, da das
Resultat der Tätigkeit gleich einem Bedarf der Tätigkeit ist und sich
nur die Lokalität des Mittels verändert.


Von hier ab weiter zu dem Bedarf an ‚Keilrahmen‘ (M2c): Eine Möglichkeit den Bedarf zu decken kann wieder die Ortsveränderung eines bereits vorhandenen Mittels sein (#OKr, T1b1d1). Eine andere Möglichkeit wäre den ‚Keilrahmen‘ aus einer zur Verfügung stehenden (a) ‚benutzten auf Keilrahmen gespannten Leinwand‘ mit einem (b) ‚Messer‘ herauszutrennen (#HtKr, T1b1d2). Schließlich kann der Bedarf nach Keilrahmen natürlich auch über deren Herstellung gedeckt werden. Das Tätigkeitsmuster \#HstKr (T1b1d3) hieße entsprechend: „Herstellung eines Keilrahmens durch (a) Winkelsäge, (b) Holzleisten, (c) Nägel und (d) Werkbank.“

Wenn im Beispiel jeder Bedarf der ausgewählten Tätigkeiten (T1b1c1 und T1b1d2) verfügbar ist, sich zu diesen Tätigkeiten jemand zugeordnet hat und auch sonst kein offener Bedarf innerhalb der Konfiguration ansteht, kann die Konfiguration – die Anordnung und Auswahl der Strukturelemente – festgesetzt werden (→[Festsetzung von Konfigurationen](/docs/festsetzen-von-konfigurationen)) und der Kooperationsprozess kann von den jeweils letzten Tätigkeiten her beginnen. Damit sich dabei Konfigurationen durch den Prozess der Selbstzuordnung herausstellen können, die im jeweiligen lokalen Umfeld sinnvoll und effektiv sind, ist einer der wesentlichen Funktionen der Software. Dieser →[Konfigurationsprozess](/docs/konfigurationsprozess) ist Schwerpunkt des dritten Teils der Textreihe.
