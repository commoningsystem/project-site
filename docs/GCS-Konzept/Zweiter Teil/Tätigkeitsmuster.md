---
sidebar_position: 2
slug: /taetigkeitsmuster
---

Die Software selbst unterstützt Prozesse zur allgemeinen Bedürfnisbefriedigung. Das heißt, es gibt Bedürfnisse die befriedigt bzw. - von einem anderen Standpunkt betrachtet – Leiden die gelindert werden wollen. Oder noch ein wenig allgemeiner formuliert: Es gibt Probleme, die gelöst werden müssen. Und um diese Probleme zu lösen muss etwas getan werden. Und wenn wir etwas machen, dann brauchen wir oft Dinge dafür und wenn wir diese Dinge nicht haben, dann ist das ein weiteres Problem, das gelöst werden muss. Ein *Tätigkeitsmuster* beschreibt dabei, was getan werden *kann*, um ein bestimmtes Problem zu lösen und welche Dinge bei dieser *Möglichkeit einer Lösung* benötigt werden.


Ein Tätigkeitsmuster ist dabei eine *individuelle Erfahrung*, welche *gesellschaftlich geteilt* und damit *frei verfügbar* gemacht wird. Auf diese Weise kann auch seine Entstehung verstanden werden: Jemand vermittelt den Bedarf nach einem bestimmten Mittel und eine andere Person weiß, wie sie dieses Mittel verfügbar machen kann. Diese Person besorgt sich, was dafür notwendig ist, geht der Tätigkeit nach und deckt schließlich den Bedarf zur Zufriedenheit der Person, welche ihn vermittelt hat. Die Person, welche die Tätigkeit ausgeführt hat, beschreibt schließlich wie sie bei der Tätigkeit vorgegangen ist und was sie dafür benötigt hat in einer genormten Form und speist sie in einer Datenbank ein. Wenn anschließend der Bedarf nach demselben Mittel noch einmal vermittelt wird, kann dieses Muster abgerufen werden – neben all den anderen Mustern mit demselben Resultat – und alle anderen

Anwender:innen der Software können auf diese individuell Erfahrung zurückgreifen und – die benötigte Qualifikation vorausgesetzt – denselben Bedarf ebenfalls decken, falls ihnen die notwendigen Mittel zur Verfügung stehen.

import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

<ThemedImage
  alt="Docusaurus themed image"
  sources={{
    light: useBaseUrl('/img/Textreihe/2/light/Muster-einfach.svg'),
    dark: useBaseUrl('/img/Textreihe/2/dark/Muster-einfach_dark.svg'),
  }}
/>


Tätigkeitsmuster sind dabei Kernelemente des ununterbrochenen Commonings. Wir bauen auf dem Gedanken auf, dass sehr viele Tätigkeiten zur Lösung bestimmter Probleme im gesellschaftlichen Kooperationsprozess nicht einmalig sind, sondern sich in gleicher Form wiederholen. Der *Rahmen* eines Tätigkeitsmusters besteht aus dem *Resultat* einer Tätigkeit – also dem, was nach der Tätigkeit verfügbar ist oder durch die Tätigkeit passiert – und dem *Bedarf* einer Tätigkeit. Der Bedarf sind alle Mittel, die zur Ausführung der Tätigkeit verfügbar sein müssen. Das Tätigkeitsmuster *enthält* schließlich eine möglichst genaue Beschreibung, wie der Bedarf angewendet werden muss, um das Resultat verfügbar zu machen.[1]

> Welche Form diese Beschreibung hat – ob Text, Audio, Video, etc. -
ist dabei selbstverständlich nicht vorgegeben. Tätigkeitsmuster, wenn
auch nicht in verarbeitbarer Form, finden sich heute bereits auf
[wikihow](https://de.wikihow.com/Hauptseite).

Das Resultat vieler Tätigkeiten (insofern es keine direkte Bedürfnisbefriedigung ist) und der Bedarf jeder Tätigkeit, ist dabei immer ein Mittel, welches auf Softwareebene durch ein *Mittelmuster bzw. Muster eines Mittels* dargestellt wird und von dort auf konkret vorhandene Mittel verweist. Der Zusammenhang des Mittelmusters mit dem konkreten Mittel wird im sechsten Teil der Textreihe, →[*Muster von Mitteln*], näher ausgearbeitet.

Neben Resultat, Bedarf und der Tätigkeitsbeschreibung ist jedes Muster noch mit weiteren Eigenschaften beschrieben, auf welche im Verlauf der Textreihe noch näher eingegangen werden wird. Für den *Konfigurationsprozess* wichtig ist dabei der *Aufwand* einer Tätigkeit (→ [*Aufwandsbestimmung*](/docs/aufwand-und-einheitsbestimmung)), welcher zusammen mit den zur Bedarfsdeckung notwendigen Tätigkeiten den *(spekulativen)* *Gesamtaufwand *ergibt (→ *Konfigurationsprozess*). Weiter gibt es eine *Musterbewertung –* sowohl die Bewertung der darin beschrieben Tätigkeit selbst, die Bewertung des hervorgehenden Resultates sowie die Auswirkungen der Tätigkeit, welche nicht als Resultat gefasst werden können (Umweltauswirkung, etc.). Falls eine →[*Qualifikation*](/docs/qualifikationen) zur Ausführung der Tätigkeit notwendig sein sollte, muss diese selbstverständlich erreicht sein, bevor sich einem Tätigkeitsmuster zugeordnet werden kann. Der Begriff der *→Fähigkeiten* verweist dabei darauf, dass es sich um ein *komplexes Tätigkeitsmuster* handelt, welches in einzelne Muster aufgeschlüsselt werden kann. Weiter relevant sind auch *Variationen von Tätigkeitsmustern* und *Nebenresultate*, wie die Abnutzung von Mitteln durch die Tätigkeit – darauf wird im sechsten Teil (→ [*Details zu Tätigkeitsmustern*]) näher eingegangen. Im Verlauf der Textreihe werden immer nur die Eigenschaften eines Tätigkeitsmusters dargestellt, welche im jeweiligen Kontext notwendig sind.
