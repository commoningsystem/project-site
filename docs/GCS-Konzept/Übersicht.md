---
sidebar_position: 1
slug: /gcs-konzept-uebersicht
---

Die hier dargestellten Teile der Textreihe sind leider nicht mehr unbedingt aktuell und der Aufwand, sie aktuell zu halten, ist im Schreibprozess zu hoch. Eine aktuelle Arbeitsversion findest du immer auf unserer [Hauptseite](https://commoningsystem.org/de/system/).

## 1. Teil

*[Theorie-Vorwissen (I)](/docs/strukturformel). [Strukturformel des Commonings](/docs/strukturformel). [Grundlagen der Softwarestruktur](/docs/grundlagen-softwarestruktur)*

(1) **Zweck der Software**, Form der damit zu erreichenden gesellschaftlichen Veränderung, **Theorie-Kontext** der Textreihe. (2) Der Prozess des Commonings als **Strukturformel** vom vermittelten Bedürfnis bis zu seiner Befriedigung. (3) Übertragung der Strukturformel auf **Softwarestruktur** und Möglichkeiten, welche sie Anwender:innen eröffnet.

<a
  target="_blank"
  href={require('/pdf/GCS/01-Grundlagen-Softwarekonzept.pdf').default}>
  [Teil 1 als pdf herunterladen]
</a>



## 2. Teil

*[Das Tätigkeitsmuster](/docs/taetigkeitsmuster). [Konfigurationen](/docs/konfigurationen). [Fähigkeiten](/docs/faehigkeiten). [Qualifikationen](/docs/qualifikationen).*

(1) **Tätigkeitsmuster** als gesellschaftlich geteilte individuelle Erfahrung sind die elementaren Teile des ununterbrochenen Commonings. Sie werden gerahmt durch den Bedarf an Mitteln, welche für die Tätigkeit benötigt werden und das Resultat, welches daraus entsteht. (2) Über Resultat und Bedarf werden Tätigkeitsmuster miteinander verbunden. Für jeden Bedarf gibt es verschiedene Möglichkeiten der Deckung, wodurch unzählige Möglichkeiten entstehen, wie jedes Bedürfnis befriedigt werden kann. Eine konkrete Auswahl und Anordnung von Tätigkeitsmustern wird **Konfiguration** genannt. (3) Tätigkeiten werden durch Personen durchgeführt, welche zu diesen befähigt sind. Diese **Fähigkeiten** werden durch die Beteiligten selbst in der Form von Tätigkeitsmuster in einer Bibliothek definiert. Komplexe Tätigkeitsmuster sind dabei eine Verschachtelung einfacher Tätigkeitsmuster. (4) Zu unterscheiden sind die Fähigkeiten von der **Qualifikation**, welche durch andere erteilt werden muss und welche Bedingung für die Zuordnung zu Tätigkeiten ist.

<a
  target="_blank"
  href={require('/pdf/GCS/02-Konfigurationen-Softwarekonzept.pdf').default}>
  [Teil 2 als pdf herunterladen]
</a>

## 3. Teil

[Aufwand und Einheit des Konfigurationsprozesses](/docs/aufwand-und-einheitsbestimmung). [Momente des Gesamtaufwandes](/docs/momente-des-gesamtaufwandes). [Der Konfigurationsprozess](/docs/konfigurationsprozess). [Plankonfigurationen](/docs/plankonfigurationen). [Kontinuität](/docs/kontinuitaet). [Interaktion mit Vorschlägen und Abfragen](/docs/interaktion). [Festsetzen einer Konfiguration](/docs//festsetzen-von-konfigurationen). [Der Reparaturprozess](/docs/reparaturprozess).

(1) Der **Aufwand** einer Tätigkeit kann notwendig zur Messung einer Form der *Reputation/Anerkennung* sein, als **Einheit** des Konfigurationsprozesses wurde sich nur für einen Teilaspekt des Aufwandes entschieden: Die *Dauer* von Tätigkeiten. (2) Die drei wesentlichen **Momente des Gesamtaufwandes** sind die lokale Verfügbarkeit von Mitteln und Tätigkeitsmustern, die Verfügungsmöglichkeiten konkreter Personen über Mittel und Nebenresultate von ausgeführten Tätigkeiten inklusive ihrer Auswirkung auf Mittel. (3) Grundlegende für die angestrebte Form der Vermittlung von Tätigkeiten ist die durch Selbstauswahl gestützte Selbstorganisation, welche erst durch den **Konfigurationsprozess** - in welchem durch die Software Tätigkeiten *vorgeschlagen* werden - effizient werden kann. Konfigurationen werden dabei durch die Verfügbarkeit von Mitteln und darüber getroffene Absprachen gerahmt und durch die Software vorgeschlagene Tätigkeiten werden ihrem Zweck nach geteilt in (a) Bedarfdeckung und (b) der Wiederherstellung von definierten Mittelzuständen. Ein weiterer wesentlicher Aspekt des Konfigurationsprozesses ist die Abfrage von Wissen und der Verfügbarkeit von Mitteln, welche sich wieder auf bereits vorgeschlagene Tätigkeiten auswirken kann. (4) Konfigurationen müssen dabei nicht aus durch die Software vorgeschlagenen Tätigkeiten bestehen, sondern können auch bewusst geplant sein - wir bezeichnen das als **Plankonfigurationen**. (5) **Kontinuität** bedeutet eine wiederholte Ausführung derselben Tätigkeit, um bei der Befriedigung von mehr als einem Bedürfnis mitzuwirken. Über *kontinuierliche Tätigkeiten* werden unterschiedliche Konfigurationen strukturell vereinigt; außerdem wirken sie sich auf den Gesamtaufwand einer Tätigkeit aus. (6) Beteiligte können mit **Vorschlägen und Abfragen unterschiedlich interagieren**. Es geht nicht nur um zusagen und ablehnen, sondern auch den Grad der Zusage/Ablehung oder etwa herauszustellen, wie selbstverständlich es innerhalb eines bestimmten lokalen Umfeldes ist, über ein Mittel zu verfügen. (7) Bevor Beteiligte anfangen können zu kooperieren - also wirklich tätig zu werden -, muss eine **Konfiguration festgesetzt werden**. (8) Auch nach dem Festsetzen von Konfigurationen können Änderungen jener notwendig sein. Dieser Änderungsprozess wird **Reparaturprozess** genannt, wobei dieser ein Set verschiedenartiger Werkzeuge sein muss.

<a
  target="_blank"
  href={require('/pdf/GCS/03-Konfigurationsprozess-Softwarekonzept.pdf').default}>
  [Teil 3 als pdf herunterladen]
</a>


## 4.-8. Teil

*Die Textreihe ist bisher nicht abgeschlossen. Da die Software-Struktur zwar durch den ['Timeless Way of Re-Production'](/docs/timeless-way-of-re-production) konzeptionell weitgehend feststeht, im Detail aber ständigen Veränderungen durch neue Erkenntnisse oder Diskussionen unterliegt, kann über die nachfolgenden Teile keine exakte Aussage getroffen werden. Die folgenden Überlegungen sind von Mitte 2020.*

4. Teil: Theorie-Vorwissen (II). Commons im sozialen Prozess. Reputation. Wichtigkeit. (1) [Transformation] Kontext von Commoning zur Lohnarbeit. Strukturunterschiede, Krise und Wechsel der sozialen Form der Mittel. (2) Commons sind nicht einfach verplanbar – um ihre Verwendung entsteht immer ein sozialer Prozess. Der soziale Prozess geschieht außerhalb der Software, kann durch diese aber unterstützt werden. (3) Der soziale Prozess geschieht auf Augenhöhe, allerdings kann Reputation zur Entscheidungsfindung beitragen. Auf Softwareebene kann Reputation etwa durch den eigens geleisteten Aufwand für andere transparent gemacht werden. (4) Die Wichtigkeit einer Tätigkeit trifft u.a. eine Aussage darüber, wie viele Bedürfnisse sie befriedigt und kann ebenfalls wesentliches Indiz zur Entscheidungsfindung zur Verwendung von Commons sein. (Anmerkung 2021: schon hier gibt es wesentliche Unterschiede in der Konzeption heute. Siehe insbesondere [Berücksichtigungen](https://community.commoningsystem.org/t/selbstbestimmte-berucksichtigung-von-anerkennung-lebensumstanden-und-sozialen-beziehungen/157))

5. Teil: Identifikation und Vertrauen. Nutzungsbedingungen von Mitteln. Transparenz von Absprachen und Regeln. Sanktionen.  (1) Es braucht Möglichkeiten der eindeutigen Identifikation von Beteiligten im Rahmen des Re-Produktionsprozesses und der Aussprache von Vertrauen zwischen Beteiligten. (2) Beteiligte können private Mittel für Commoning unter ihren eigenen Nutzungsbedingungen zur Verfügung stellen, aber auch gesellschaftliche Mittel unterliegen solchen Regelungen. (3) Commoning basiert auf gemeinsamen Absprachen und Regelsetzungen, welche über die Software transparent gemacht werden können. (4) Es braucht weiter Möglichkeiten, wie abgestufte Sanktionen ausgesprochen werden können, wenn mit den Regeln gebrochen wird. (5) Über Tätigkeitsmuster wird ein Re-Produktionsprozess zwar transparent, allerdings sind die Muster selbst statisch. Zusammenschlüsse, welche außerhalb der Software entstehen, können in die Software integriert werden. Auf Strukturebene ähneln sie dabei Tätigkeitsmustern – sie sind durch Resultat und Bedarf gerahmt – wenn gleich nicht transparent ist, wie sie das Resultat hervorbringen etc.

6. Teil: Interpersonaler Beziehungen und Kommunikation. Integrierte Zusammenschlüsse. Sprachfähigkeit erarbeiten

7. Teil: Vermittlung von Bedürfnissen. Muster von Mitteln. Erhaltung und Pflege von Commons. Details zu Tätigkeitsmustern. (1) Auf Softwareebene gehen wir nicht mit konkreten Mitteln um, sondern ebenfalls mit Mustern von Mitteln. Diese Muster müssen so angelegt werden, dass sie konkrete Mittel mit ihren spezifischen Nützlichkeiten fassen können, damit die Konfigurationen selbst sinnvoll entstehen können. (2) Commons müssen gepflegt, repariert, gewartet etc. werden – das kann bedeuten: Sie müssen sich anhand definierter Regeln in den Konfigurationsprozess einbringen können. (3) Bedürfnisse werden vermittelt, müssen aber durch die Software sinnvoll ausgelesen werden. Auch hier müssen schon Absprachen mit anderen getroffen werden können, deren Bedürfnisse auf ähnliche Mittel verweisen. (4) Nachdem Tätigkeitsmuster zu Beginn rudimentär eingeführt wurden, wird es noch einmal in seiner ganzen Ausführlichkeit betrachtet.

8. Teil: Anforderungen an die Software. Zuletzt werden Anforderungen gesetzt, welche bei der Entwicklung unserer Ansicht nach unbedingte Priorität haben. Allem voran steht die Lizenz als Freie Software.
